package az.ibar.taskify.ms.identity.service;

import az.ibar.taskify.lib.common.component.security.TokenProvider;
import az.ibar.taskify.ms.identity.component.mapper.TokenMapper;
import az.ibar.taskify.ms.identity.component.security.TokenHelper;
import az.ibar.taskify.ms.identity.model.dto.request.LoginDto;
import az.ibar.taskify.ms.identity.model.dto.request.RefreshDto;
import az.ibar.taskify.ms.identity.model.dto.response.TokenDto;
import az.ibar.taskify.ms.identity.model.hash.Token;
import az.ibar.taskify.ms.identity.repository.redis.TokenRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;

import java.util.Optional;

import static az.ibar.taskify.ms.identity.util.AuthUtils.buildAuthentication;
import static az.ibar.taskify.ms.identity.util.AuthUtils.buildLoginDto;
import static az.ibar.taskify.ms.identity.util.AuthUtils.buildRefreshDto;
import static az.ibar.taskify.ms.identity.util.AuthUtils.buildToken;
import static az.ibar.taskify.ms.identity.util.AuthUtils.buildTokenDto;
import static az.ibar.taskify.ms.identity.util.CommonUtils.ACCESS_TOKEN;
import static az.ibar.taskify.ms.identity.util.CommonUtils.AUTHORIZATION_HEADER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthServiceTest {

    @InjectMocks
    private AuthService authService;

    @Mock
    private TokenMapper tokenMapper;

    @Mock
    private TokenHelper tokenHelper;

    @Mock
    private TokenProvider tokenProvider;

    @Mock
    private TokenRepository tokenRepository;

    @Mock
    private AuthenticationManager authenticationManager;


    @Test
    void givenCorrectParamsWhenLoginThenReturnTokenDto() {
        //Arrange
        LoginDto loginDto = buildLoginDto();
        Token token = buildToken();
        TokenDto tokenDto = buildTokenDto();
        Authentication authentication = buildAuthentication();

        when(authenticationManager.authenticate(authentication)).thenReturn(authentication);
        when(tokenHelper.createToken(authentication)).thenReturn(token);
        when(tokenRepository.save(token)).thenReturn(token);
        when(tokenMapper.hash2Dto(token)).thenReturn(tokenDto);

        //Act
        TokenDto actualTokenDto = authService.login(loginDto);

        //Assert
        assertThat(actualTokenDto).isEqualTo(tokenDto);
    }

    @Test
    void givenCorrectParamsWhenLogoutThenReturnOk() {
        //Arrange
        Authentication authentication = buildAuthentication();
        when(tokenProvider.extractToken(AUTHORIZATION_HEADER)).thenReturn(Optional.of(ACCESS_TOKEN));
        when(tokenProvider.parseAuthentication(ACCESS_TOKEN))
                .thenReturn(authentication);

        //Act
        authService.logout(AUTHORIZATION_HEADER);

        //Verify
        verify(tokenRepository).deleteById(authentication.getName());
    }

    /*@Test
    void givenInvalidTokenWhenLogoutThenThrowInvalidTokenException() {
        //Arrange
        Authentication authentication = buildAuthentication();
        when(tokenProvider.extractToken(AUTHORIZATION_HEADER))
                .thenThrow(InvalidTokenException.class);

        //Act
        authService.logout(null);

        //Verify
        verify(tokenRepository).deleteById(authentication.getName());
    }*/

    @Test
    void givenCorrectParamsWhenRefreshThenReturnRefreshDto() {
        //Arrange
        RefreshDto refreshDto = buildRefreshDto();
        Authentication authentication = buildAuthentication();
        Token token = buildToken();
        TokenDto tokenDto = buildTokenDto();

        when(tokenProvider.parseAuthentication(refreshDto.getRefreshToken())).thenReturn(authentication);
        when(tokenRepository.findById(authentication.getName())).thenReturn(Optional.of(token));
        when(tokenHelper.createToken(authentication)).thenReturn(token);
        when(tokenRepository.save(token)).thenReturn(token);
        when(tokenMapper.hash2Dto(token)).thenReturn(tokenDto);

        //Act
        TokenDto actualTokenDto = authService.refresh(refreshDto);

        //Assert
        assertThat(actualTokenDto).isEqualTo(tokenDto);
    }


}
