//package az.ibar.taskify.ms.identity.service;
//
//import az.ibar.taskify.ms.identity.model.entity.Role;
//import az.ibar.taskify.ms.identity.model.entity.UserRole;
//import az.ibar.taskify.ms.identity.repository.RoleRepository;
//import az.ibar.taskify.ms.identity.repository.UserRoleRepository;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import java.time.LocalDateTime;
//
//import static az.ibar.taskify.ms.identity.util.CommonUtils.ID;
//
//@ExtendWith(MockitoExtension.class)
//public class UserRoleServiceTest {
//
//    @InjectMocks
//    private UserRoleService userRoleService;
//
//    @Mock
//    private RoleRepository roleRepository;
//
//    @Mock
//    private UserRoleRepository userRoleRepository;
//
//    private UserRole userRole;
//    private Role role;
//
//    @BeforeEach
//    void setUp() {
//        role = new Role();
//        role.setId(ID);
//        role.setCreatedDate(LocalDateTime.now());
//        role.setActive(true);
//        role.setDescription("Description");
//
//        userRole = UserRole.builder()
//                .id(ID)
//                .roleId(role.getId())
//                .userId(ID)
//                .createdDate(LocalDateTime.now())
//                .active(true)
//                .build();
//    }
//
//    /*@Test
//    void createAdminUserRoleTest() {
//        when(roleRepository.findAllByRoleIn(List.of(RoleName.ROLE_ADMIN, RoleName.ROLE_USER)))
//                .thenReturn(List.of(role));
//        //when(userRoleService.buildUserRole(ID, role.getId()))
//              //  .thenReturn(userRole);
//
//        //Act
//        userRoleService.createAdminUserRole(ID);
//
//        //Verify
//        verify(userRoleRepository).saveAll(List.of(userRole));
//    }*/
//}
