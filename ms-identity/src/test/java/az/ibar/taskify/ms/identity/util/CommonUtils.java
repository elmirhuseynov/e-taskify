package az.ibar.taskify.ms.identity.util;

public final class CommonUtils {

    public static final Long ID = -1L;
    public static final String EMAIL = "test@test.com";
    public static final String PASSWORD = "StrongPassword!@#1";
    public static final String ENCODED_PASSWORD = "ENCODED_PASSWORD";
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
    public static final String AUTHORIZATION_HEADER = "Bearer TOKEN";

    private CommonUtils() {
    }
}
