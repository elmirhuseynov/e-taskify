package az.ibar.taskify.ms.identity.controller;

import az.ibar.taskify.lib.common.component.MessageResolver;
import az.ibar.taskify.lib.common.component.security.AccessDeniedHandlerImpl;
import az.ibar.taskify.lib.common.component.security.AuthenticationEntryPointImpl;
import az.ibar.taskify.lib.common.component.security.TokenProvider;
import az.ibar.taskify.ms.identity.component.security.AuthenticationProviderImpl;
import az.ibar.taskify.ms.identity.exception.UserNotFoundException;
import az.ibar.taskify.ms.identity.model.dto.request.UserRegistrationDto;
import az.ibar.taskify.ms.identity.model.dto.response.UserInfoDto;
import az.ibar.taskify.ms.identity.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.filter.CorsFilter;

import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.ERROR;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.PATH;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.STATUS;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.TIMESTAMP;
import static az.ibar.taskify.ms.identity.util.CommonUtils.ID;
import static az.ibar.taskify.ms.identity.util.UserUtils.buildUserInfoDto;
import static az.ibar.taskify.ms.identity.util.UserUtils.buildUserRegistrationDto;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
@AutoConfigureMockMvc(addFilters = false)
@SuppressWarnings("PMD.UnusedPrivateField")
public class UserControllerTest {

    private static final String BASE_REQUEST_PATH = "/user";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;

    @MockBean
    private CorsFilter corsFilter;

    @MockBean
    private TokenProvider tokenProvider;

    @MockBean
    private MessageResolver messageResolver;

    @MockBean
    private AccessDeniedHandlerImpl accessDeniedHandler;

    @MockBean
    private AuthenticationProviderImpl authenticationProvider;

    @MockBean
    private AuthenticationEntryPointImpl authenticationEntryPoint;

    @Test
    @WithMockUser(roles = "ADMIN")
    void givenCorrectParamWhenRegistrationThenResponseIsOk() throws Exception {
        //Arrange
        UserRegistrationDto userRegistrationDto = buildUserRegistrationDto();
        doNothing().when(userService).registerUser(userRegistrationDto);

        mockMvc.perform(post(BASE_REQUEST_PATH.concat("/registration"))
                .content(objectToJson(userRegistrationDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());
    }

    @Test
    void givenNotAdminUserWhenRegistrationThenResponseIsForbidden() throws Exception {
        //Arrange
        String path = BASE_REQUEST_PATH.concat("/registration");
        UserRegistrationDto userRegistrationDto = buildUserRegistrationDto();
        doNothing().when(userService).registerUser(userRegistrationDto);

        mockMvc.perform(post(path)
                .content(objectToJson(userRegistrationDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isForbidden())
                .andExpect(jsonPath(STATUS, is(HttpStatus.FORBIDDEN.value())))
                .andExpect(jsonPath(TIMESTAMP).isNotEmpty())
                .andExpect(jsonPath(ERROR, is(HttpStatus.FORBIDDEN.getReasonPhrase())))
                .andExpect(jsonPath(PATH, is(path)));
    }

    @Test
    void givenCorrectParamWhenUserInfoThenResponseIsOk() throws Exception {
        //Arrange
        UserInfoDto userInfoDto = buildUserInfoDto();
        doReturn(userInfoDto).when(userService).getUserInfo(ID);

        mockMvc.perform(get(BASE_REQUEST_PATH.concat("/info"))
                .param("userId", String.valueOf(ID))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());
    }

    @Test
    void givenInCorrectParamWhenUserInfoThenResponseIsBadRequest() throws Exception {
        //Arrange
        String path = BASE_REQUEST_PATH.concat("/info");
        doThrow(new UserNotFoundException()).when(userService).getUserInfo(any());

        mockMvc.perform(get(path)
                .param("userId", String.valueOf(ID))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(STATUS, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(TIMESTAMP).isNotEmpty())
                .andExpect(jsonPath(ERROR, is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath(PATH, is(path)));
    }

    private String objectToJson(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }

}
