package az.ibar.taskify.ms.identity.util;

import az.ibar.taskify.ms.identity.model.dto.request.CustomerDto;
import az.ibar.taskify.ms.identity.model.dto.request.RegistrationDto;
import az.ibar.taskify.ms.identity.model.dto.request.UserDto;

import static az.ibar.taskify.ms.identity.util.CustomerUtils.buildCustomerDto;
import static az.ibar.taskify.ms.identity.util.UserUtils.buildUserDto;

@SuppressWarnings("MethodLength")
public final class RegistrationUtils {

    public static RegistrationDto buildRegistrationDto() {
        CustomerDto customerDto = buildCustomerDto();
        UserDto userDto = buildUserDto();

        var registrationDto = new RegistrationDto();
        registrationDto.setCustomer(customerDto);
        registrationDto.setUser(userDto);

        return registrationDto;
    }

    private RegistrationUtils() {
    }
}
