package az.ibar.taskify.ms.identity.service;

import az.ibar.taskify.ms.identity.component.MessagePublisher;
import az.ibar.taskify.ms.identity.component.mapper.UserMapper;
import az.ibar.taskify.ms.identity.exception.EmailAlreadyExistException;
import az.ibar.taskify.ms.identity.model.constant.IdentityResponseMessage;
import az.ibar.taskify.ms.identity.repository.CustomerRepository;
import az.ibar.taskify.ms.identity.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static az.ibar.taskify.ms.identity.util.CommonUtils.EMAIL;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserMapper userMapper;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserRoleService userRoleService;

    @Mock
    private MessagePublisher messagePublisher;

    @Mock
    private CustomerRepository customerRepository;

    @Test
    void testFindUserByEmail() {
        //Act
        userService.findUserByEmail(EMAIL);

        //Verify
        verify(userRepository).findByEmail(EMAIL);
    }

    @Test
    void givenAlreadyExistEmailThenThrowEx() {
        //Arrange
        when(userRepository.findByEmail(EMAIL)).thenThrow(new EmailAlreadyExistException(EMAIL));

        //Act & Assert
        assertThatThrownBy(() -> userService.throwExWhenEmailAlreadyExist(EMAIL))
                .isInstanceOf(EmailAlreadyExistException.class)
                .hasMessage(IdentityResponseMessage.EMAIL_ALREADY_EXIST.getMessageKey());
    }

    /*@Test
    void testRegisterUser() {
        //Arrange
        when(customerRepository.findById(ID)).thenReturn(Optional.of(customer));
        when(userMapper.userRegistrationDto2Entity(userRegistrationDto, customer, PASSWORD))
                .thenReturn(user);
        when(userRepository.save(user)).thenReturn(user);

        //Act
        userService.registerUser(userRegistrationDto);

        //Verify
        verify(messagePublisher).publish(EXCHANGE_MAIL, ROUTING_KEY_MAIL, new MailEvent());
    }*/

}
