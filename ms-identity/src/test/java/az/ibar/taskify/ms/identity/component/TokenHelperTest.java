package az.ibar.taskify.ms.identity.component;

import az.ibar.taskify.lib.common.component.security.TokenProvider;
import az.ibar.taskify.lib.common.configuration.properties.SecurityProperties;
import az.ibar.taskify.ms.identity.component.security.TokenHelper;
import az.ibar.taskify.ms.identity.model.hash.Token;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;

import java.time.Duration;

import static az.ibar.taskify.ms.identity.util.AuthUtils.buildAuthentication;
import static az.ibar.taskify.ms.identity.util.AuthUtils.buildToken;
import static az.ibar.taskify.ms.identity.util.CommonUtils.ACCESS_TOKEN;
import static az.ibar.taskify.ms.identity.util.CommonUtils.ID;
import static az.ibar.taskify.ms.identity.util.CommonUtils.REFRESH_TOKEN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TokenHelperTest {

    @InjectMocks
    private TokenHelper tokenHelper;

    @Mock
    private TokenProvider tokenProvider;

    @Mock
    private SecurityProperties securityProperties;

    private SecurityProperties.JwtProperties jwtProperties;

    @BeforeEach
    void setUp() {
        jwtProperties = new SecurityProperties.JwtProperties();
        jwtProperties.setTimeToLiveInCache(Duration.ofSeconds(ID));
    }

    @Test
    void givenCorrectParamsWhenCreateTokenThenReturnTokenDto() {
        //Arrange
        Token token = buildToken();
        Authentication authentication = buildAuthentication();
        when(tokenProvider.createAccessToken(authentication)).thenReturn(ACCESS_TOKEN);
        when(tokenProvider.createRefreshToken(authentication)).thenReturn(REFRESH_TOKEN);
        when(securityProperties.getJwtProperties()).thenReturn(jwtProperties);


        //Act
        Token actualToken = tokenHelper.createToken(authentication);

        //Assert
        assertThat(actualToken).isEqualTo(token);
    }

}
