package az.ibar.taskify.ms.identity.service;

import az.ibar.taskify.ms.identity.component.mapper.CustomerMapper;
import az.ibar.taskify.ms.identity.component.mapper.UserMapper;
import az.ibar.taskify.ms.identity.model.dto.request.CustomerDto;
import az.ibar.taskify.ms.identity.model.dto.request.RegistrationDto;
import az.ibar.taskify.ms.identity.model.dto.request.UserDto;
import az.ibar.taskify.ms.identity.model.entity.Customer;
import az.ibar.taskify.ms.identity.model.entity.User;
import az.ibar.taskify.ms.identity.repository.CustomerRepository;
import az.ibar.taskify.ms.identity.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static az.ibar.taskify.ms.identity.util.CustomerUtils.buildCustomer;
import static az.ibar.taskify.ms.identity.util.RegistrationUtils.buildRegistrationDto;
import static az.ibar.taskify.ms.identity.util.UserUtils.buildUser;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RegistrationServiceTest {

    @InjectMocks
    private RegistrationService registrationService;

    @Mock
    private UserMapper userMapper;

    @Mock
    private UserService userService;

    @Mock
    private CustomerMapper customerMapper;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserRoleService userRoleService;

    @Mock
    private CustomerRepository customerRepository;

    @Test
    void givenCorrectParamsWhenSignupThenReturnRegistrationDto() {
        //Arrange
        RegistrationDto registrationDto = buildRegistrationDto();
        CustomerDto customerDto = registrationDto.getCustomer();
        UserDto userDto = registrationDto.getUser();
        User user = buildUser();
        Customer customer = buildCustomer();

        when(customerMapper.dto2Entity(customerDto)).thenReturn(customer);
        when(customerRepository.save(customer)).thenReturn(customer);
        when(userMapper.dto2Entity(userDto, customer)).thenReturn(user);
        when(userRepository.save(user)).thenReturn(user);

        //Act
        registrationService.signup(registrationDto);

        //Verify
        verify(userService).throwExWhenEmailAlreadyExist(user.getEmail());
        verify(userRoleService).createAdminUserRole(user.getId());
    }

}
