package az.ibar.taskify.ms.identity.util;

import az.ibar.taskify.ms.identity.model.constant.CustomerState;
import az.ibar.taskify.ms.identity.model.dto.request.CustomerDto;
import az.ibar.taskify.ms.identity.model.entity.Customer;

import java.time.LocalDateTime;

import static az.ibar.taskify.ms.identity.util.CommonUtils.ID;

public final class CustomerUtils {

    private static final String ADDRESS = "Address";
    private static final String ORGANIZATION_NAME = "Organization name";
    private static final String PHONE_NUMBER = "0555555555";

    public static CustomerDto buildCustomerDto() {
        var customerDto = new CustomerDto();
        customerDto.setAddress(ADDRESS);
        customerDto.setOrganizationName(ORGANIZATION_NAME);
        customerDto.setPhoneNumber(PHONE_NUMBER);

        return customerDto;
    }

    public static Customer buildCustomer() {
        return Customer.builder()
                .id(ID)
                .address(ADDRESS)
                .createdDate(LocalDateTime.now())
                .organizationName(ORGANIZATION_NAME)
                .phoneNumber(PHONE_NUMBER)
                .customerState(CustomerState.ACTIVE)
                .build();
    }

    private CustomerUtils() {
    }
}
