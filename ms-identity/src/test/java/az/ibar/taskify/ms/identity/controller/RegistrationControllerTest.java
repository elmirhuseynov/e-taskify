package az.ibar.taskify.ms.identity.controller;

import az.ibar.taskify.lib.common.component.MessageResolver;
import az.ibar.taskify.lib.common.component.security.AccessDeniedHandlerImpl;
import az.ibar.taskify.lib.common.component.security.AuthenticationEntryPointImpl;
import az.ibar.taskify.lib.common.component.security.TokenProvider;
import az.ibar.taskify.ms.identity.component.security.AuthenticationProviderImpl;
import az.ibar.taskify.ms.identity.exception.EmailAlreadyExistException;
import az.ibar.taskify.ms.identity.model.dto.request.RegistrationDto;
import az.ibar.taskify.ms.identity.service.RegistrationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.filter.CorsFilter;

import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.ERROR;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.PATH;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.STATUS;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.TIMESTAMP;
import static az.ibar.taskify.ms.identity.util.CommonUtils.EMAIL;
import static az.ibar.taskify.ms.identity.util.RegistrationUtils.buildRegistrationDto;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(RegistrationController.class)
@AutoConfigureMockMvc(addFilters = false)
@SuppressWarnings("PMD.UnusedPrivateField")
public class RegistrationControllerTest {

    private static final String BASE_REQUEST_PATH = "/registration";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RegistrationService registrationService;

    @MockBean
    private CorsFilter corsFilter;

    @MockBean
    private TokenProvider tokenProvider;

    @MockBean
    private MessageResolver messageResolver;

    @MockBean
    private AccessDeniedHandlerImpl accessDeniedHandler;

    @MockBean
    private AuthenticationProviderImpl authenticationProvider;

    @MockBean
    private AuthenticationEntryPointImpl authenticationEntryPoint;


    @Test
    void givenCorrectParamWhenSignupThenResponseIsOk() throws Exception {
        //Arrange
        RegistrationDto registrationDto = buildRegistrationDto();
        doNothing().when(registrationService).signup(registrationDto);

        mockMvc.perform(post(BASE_REQUEST_PATH.concat("/signup"))
                .content(objectToJson(registrationDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());
    }

    @Test
    void givenAlreadyExistEmailWhenSignupThenResponseIsBadRequest() throws Exception {
        //Arrange
        String path = BASE_REQUEST_PATH.concat("/signup");
        RegistrationDto registrationDto = buildRegistrationDto();
        doThrow(new EmailAlreadyExistException(EMAIL)).when(registrationService).signup(registrationDto);

        mockMvc.perform(post(path)
                .content(objectToJson(registrationDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(STATUS, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(TIMESTAMP).isNotEmpty())
                .andExpect(jsonPath(ERROR, is(HttpStatus.BAD_REQUEST.getReasonPhrase())))
                .andExpect(jsonPath(PATH, is(path)));
    }

    private String objectToJson(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }
}
