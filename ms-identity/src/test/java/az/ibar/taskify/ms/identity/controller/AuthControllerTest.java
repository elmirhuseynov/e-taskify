package az.ibar.taskify.ms.identity.controller;

import az.ibar.taskify.lib.common.component.MessageResolver;
import az.ibar.taskify.lib.common.component.security.AccessDeniedHandlerImpl;
import az.ibar.taskify.lib.common.component.security.AuthenticationEntryPointImpl;
import az.ibar.taskify.lib.common.component.security.TokenProvider;
import az.ibar.taskify.ms.identity.component.security.AuthenticationProviderImpl;
import az.ibar.taskify.ms.identity.exception.InvalidAccessTokenException;
import az.ibar.taskify.ms.identity.exception.InvalidCredentialsException;
import az.ibar.taskify.ms.identity.exception.InvalidRefreshTokenException;
import az.ibar.taskify.ms.identity.model.dto.request.LoginDto;
import az.ibar.taskify.ms.identity.model.dto.request.RefreshDto;
import az.ibar.taskify.ms.identity.model.dto.response.TokenDto;
import az.ibar.taskify.ms.identity.service.AuthService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.filter.CorsFilter;

import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.ERROR;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.PATH;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.STATUS;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.TIMESTAMP;
import static az.ibar.taskify.ms.identity.util.AuthUtils.buildLoginDto;
import static az.ibar.taskify.ms.identity.util.AuthUtils.buildRefreshDto;
import static az.ibar.taskify.ms.identity.util.AuthUtils.buildTokenDto;
import static az.ibar.taskify.ms.identity.util.CommonUtils.AUTHORIZATION_HEADER;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(AuthController.class)
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SuppressWarnings("PMD.UnusedPrivateField")
public class AuthControllerTest {

    private static final String BASE_REQUEST_PATH = "/auth";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AuthService authService;

    @MockBean
    private CorsFilter corsFilter;

    @MockBean
    private TokenProvider tokenProvider;

    @MockBean
    private MessageResolver messageResolver;

    @MockBean
    private AccessDeniedHandlerImpl accessDeniedHandler;

    @MockBean
    private AuthenticationProviderImpl authenticationProvider;

    @MockBean
    private AuthenticationEntryPointImpl authenticationEntryPoint;

    @Test
    void givenCorrectParamWhenLoginThenResponseIsOk() throws Exception {
        //Arrange
        LoginDto loginDto = buildLoginDto();
        TokenDto tokenDto = buildTokenDto();
        when(authService.login(loginDto)).thenReturn(tokenDto);

        mockMvc.perform(post(BASE_REQUEST_PATH.concat("/login"))
                .content(objectToJson(loginDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(content().json(objectToJson(tokenDto)));
    }

    @Test
    void givenBadCredentialsParamWhenLoginThenResponseIsBadRequest() throws Exception {
        //Arrange
        LoginDto loginDto = buildLoginDto();
        String path = BASE_REQUEST_PATH.concat("/login");
        doThrow(new InvalidCredentialsException()).when(authService).login(loginDto);

        mockMvc.perform(post(path)
                .content(objectToJson(loginDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(STATUS, is(HttpStatus.BAD_REQUEST.value())))
                //.andExpect(jsonPath(MESSAGE, is(IdentityResponseStatus.INVALID_USER_CREDENTIALS.getMessageKey())))
                .andExpect(jsonPath(TIMESTAMP).isNotEmpty())
                .andExpect(jsonPath(ERROR, is(HttpStatus.BAD_REQUEST.getReasonPhrase())))
                .andExpect(jsonPath(PATH, is(path)));
    }

    @Test
    void givenCorrectParamWhenLogoutThenResponseIsOk() throws Exception {
        //Arrange
        String path = BASE_REQUEST_PATH.concat("/logout");
        doNothing().when(authService).logout(AUTHORIZATION_HEADER);

        mockMvc.perform(get(path)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());
    }

    @Test
    void givenInvalidTokenParamWhenLogoutThenResponseIsBadRequest() throws Exception {
        //Arrange
        String path = BASE_REQUEST_PATH.concat("/logout");
        doThrow(new InvalidAccessTokenException()).when(authService).logout(AUTHORIZATION_HEADER);

        mockMvc.perform(get(path)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(STATUS, is(HttpStatus.BAD_REQUEST.value())))
                //.andExpect(jsonPath(MESSAGE, is(IdentityResponseStatus.INVALID_ACCESS_TOKEN.getMessageKey())))
                .andExpect(jsonPath(TIMESTAMP).isNotEmpty())
                .andExpect(jsonPath(ERROR, is(HttpStatus.BAD_REQUEST.getReasonPhrase())))
                .andExpect(jsonPath(PATH, is(path)));
    }

    @Test
    void givenCorrectParamWhenRefreshThenResponseIsOk() throws Exception {
        //Arrange
        RefreshDto refreshDto = buildRefreshDto();
        TokenDto tokenDto = buildTokenDto();
        String path = BASE_REQUEST_PATH.concat("/refresh");
        doReturn(tokenDto).when(authService).refresh(refreshDto);

        mockMvc.perform(post(path)
                .content(objectToJson(tokenDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(content().json(objectToJson(refreshDto)));
    }

    @Test
    void givenInvalidTokenParamWhenRefreshThenResponseIsBadRequest() throws Exception {
        //Arrange
        RefreshDto refreshDto = buildRefreshDto();
        String path = BASE_REQUEST_PATH.concat("/refresh");
        doThrow(new InvalidRefreshTokenException()).when(authService).refresh(refreshDto);

        mockMvc.perform(post(path)
                .content(objectToJson(refreshDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(STATUS, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(TIMESTAMP).isNotEmpty())
                .andExpect(jsonPath(ERROR, is(HttpStatus.BAD_REQUEST.getReasonPhrase())))
                .andExpect(jsonPath(PATH, is(path)));
    }

    private String objectToJson(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }
}
