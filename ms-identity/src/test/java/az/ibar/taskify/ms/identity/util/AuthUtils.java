package az.ibar.taskify.ms.identity.util;

import az.ibar.taskify.ms.identity.model.dto.request.LoginDto;
import az.ibar.taskify.ms.identity.model.dto.request.RefreshDto;
import az.ibar.taskify.ms.identity.model.dto.response.TokenDto;
import az.ibar.taskify.ms.identity.model.hash.Token;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import static az.ibar.taskify.ms.identity.util.CommonUtils.ACCESS_TOKEN;
import static az.ibar.taskify.ms.identity.util.CommonUtils.EMAIL;
import static az.ibar.taskify.ms.identity.util.CommonUtils.ID;
import static az.ibar.taskify.ms.identity.util.CommonUtils.PASSWORD;
import static az.ibar.taskify.ms.identity.util.CommonUtils.REFRESH_TOKEN;

public final class AuthUtils {

    public static TokenDto buildTokenDto() {
        return TokenDto.builder()
                .accessToken(ACCESS_TOKEN)
                .refreshToken(REFRESH_TOKEN)
                .build();
    }

    public static LoginDto buildLoginDto() {
        LoginDto loginDto = new LoginDto();
        loginDto.setEmail(EMAIL);
        loginDto.setPassword(PASSWORD);
        return loginDto;
    }

    public static Authentication buildAuthentication() {
        return new UsernamePasswordAuthenticationToken(EMAIL, PASSWORD);
    }

    public static Token buildToken() {
        return Token.builder()
                .email(EMAIL)
                .accessToken(ACCESS_TOKEN)
                .refreshToken(REFRESH_TOKEN)
                .ttl(ID)
                .build();
    }

    public static RefreshDto buildRefreshDto() {
        RefreshDto refreshDto = new RefreshDto();
        refreshDto.setRefreshToken(REFRESH_TOKEN);
        return refreshDto;
    }

    private AuthUtils() {
    }
}
