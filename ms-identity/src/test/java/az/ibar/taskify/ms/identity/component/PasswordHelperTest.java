package az.ibar.taskify.ms.identity.component;

import az.ibar.taskify.ms.identity.component.security.PasswordHelper;
import az.ibar.taskify.ms.identity.exception.InvalidCredentialsException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import static az.ibar.taskify.ms.identity.util.CommonUtils.ENCODED_PASSWORD;
import static az.ibar.taskify.ms.identity.util.CommonUtils.PASSWORD;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PasswordHelperTest {

    @InjectMocks
    private PasswordHelper passwordHelper;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Test
    void givenCorrectParamsWhenValidatePasswordThenReturnOk() {
        //Arrange
        when(passwordEncoder.matches(PASSWORD, ENCODED_PASSWORD)).thenReturn(true);

        //Act
        passwordHelper.validatePassword(PASSWORD, ENCODED_PASSWORD);

        //Assert
        verify(passwordEncoder).matches(PASSWORD, ENCODED_PASSWORD);
    }

    @Test
    void givenIncorrectParamsWhenValidatePasswordThenThrowEx() {
        //Arrange
        when(passwordEncoder.matches(PASSWORD, ENCODED_PASSWORD))
                .thenThrow(new InvalidCredentialsException());

        //Act & Assert
        assertThatThrownBy(() -> passwordHelper.validatePassword(PASSWORD, ENCODED_PASSWORD))
                .isInstanceOf(InvalidCredentialsException.class);
    }

    @Test
    void givenCorrectParamsWhenEncodeThenReturnEncodedPassword() {
        //Arrange
        when(passwordEncoder.encode(PASSWORD)).thenReturn(ENCODED_PASSWORD);

        //Act
        String actualEncodedPassword = passwordHelper.encode(PASSWORD);

        //Assert
        assertThat(actualEncodedPassword).isEqualTo(ENCODED_PASSWORD);

        verify(passwordEncoder).encode(PASSWORD);
    }
}
