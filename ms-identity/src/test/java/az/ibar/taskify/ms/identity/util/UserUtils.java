package az.ibar.taskify.ms.identity.util;

import az.ibar.taskify.lib.common.model.CustomUserPrincipal;
import az.ibar.taskify.ms.identity.model.constant.UserState;
import az.ibar.taskify.ms.identity.model.dto.request.UserDto;
import az.ibar.taskify.ms.identity.model.dto.request.UserRegistrationDto;
import az.ibar.taskify.ms.identity.model.dto.response.UserInfoDto;
import az.ibar.taskify.ms.identity.model.entity.User;

import java.time.LocalDateTime;
import java.util.Collections;

import static az.ibar.taskify.ms.identity.util.CommonUtils.EMAIL;
import static az.ibar.taskify.ms.identity.util.CommonUtils.ID;
import static az.ibar.taskify.ms.identity.util.CommonUtils.PASSWORD;
import static az.ibar.taskify.ms.identity.util.CustomerUtils.buildCustomer;

public final class UserUtils {

    private static final String NAME = "Elmir";
    private static final String SURNAME = "Huseynov";
    private static final String CREATED_BY = "Elmir Huseynov";

    public static UserRegistrationDto buildUserRegistrationDto() {
        var userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setEmail(EMAIL);
        userRegistrationDto.setName(NAME);
        userRegistrationDto.setSurname(SURNAME);

        return userRegistrationDto;
    }

    public static UserInfoDto buildUserInfoDto() {
        var userInfoDto = new UserInfoDto();
        userInfoDto.setId(ID);
        userInfoDto.setEmail(EMAIL);
        userInfoDto.setName(NAME);
        userInfoDto.setSurname(SURNAME);

        return userInfoDto;
    }

    public static UserDto buildUserDto() {
        var userDto = new UserDto();
        userDto.setName(NAME);
        userDto.setSurname(SURNAME);
        userDto.setEmail(EMAIL);
        userDto.setPassword(PASSWORD);
        return userDto;
    }

    public static User buildUser() {
        var user = new User();
        user.setId(ID);
        user.setPassword(PASSWORD);
        user.setCustomer(buildCustomer());
        user.setEmail(EMAIL);
        user.setName(NAME);
        user.setSurname(SURNAME);
        user.setRoles(Collections.emptyList());
        user.setUserState(UserState.ACTIVE);
        user.setCreatedBy(CREATED_BY);
        user.setCreatedDate(LocalDateTime.now());

        return user;
    }

    public static CustomUserPrincipal buildCustomUserPrincipal() {
        var user = buildUser();
        var principal = new CustomUserPrincipal(user.getEmail(),
                user.getPassword(),
                Collections.emptyList()
        );
        principal.setSurname(user.getSurname());
        principal.setName(user.getName());
        principal.setId(user.getId());
        principal.setCustomerId(user.getCustomer().getId());
        return principal;
    }

    private UserUtils() {
    }
}
