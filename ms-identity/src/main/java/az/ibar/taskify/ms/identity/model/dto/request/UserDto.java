package az.ibar.taskify.ms.identity.model.dto.request;

import az.ibar.taskify.lib.common.validation.constraint.Password;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class UserDto {

    @NotBlank(message = "validation.notBlank.name")
    private String name;

    @NotBlank(message = "validation.notBlank.surname")
    private String surname;

    @Email(message = "validation.email")
    private String email;

    @Password
    @ToString.Exclude
    private String password;

}
