package az.ibar.taskify.ms.identity.model.entity;

import az.ibar.taskify.ms.identity.model.constant.RoleName;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@Table(name = "role")
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private RoleName role;

    @NotNull
    @Column(name = "description")
    private String description;

    @CreatedDate
    @Column(name = "created_date", precision = 6, updatable = false)
    private LocalDateTime createdDate;

    @Column(name = "active")
    private Boolean active;

    @PrePersist
    public void prePersist() {
        this.active = true;
    }
}
