package az.ibar.taskify.ms.identity.model.entity;

import az.ibar.taskify.ms.identity.model.constant.CustomerState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@Table
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String organizationName;

    @NotNull
    private String phoneNumber;

    @NotNull
    private String address;

    @CreatedDate
    @Column(name = "created_date", precision = 6, updatable = false)
    private LocalDateTime createdDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "state", length = 100, nullable = false)
    private CustomerState customerState;

    @PrePersist
    public void prePersist() {
        this.customerState = CustomerState.ACTIVE;
    }
}
