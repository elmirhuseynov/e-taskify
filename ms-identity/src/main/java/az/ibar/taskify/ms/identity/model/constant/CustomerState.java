package az.ibar.taskify.ms.identity.model.constant;

public enum CustomerState {

    ACTIVE,
    DELETED

}
