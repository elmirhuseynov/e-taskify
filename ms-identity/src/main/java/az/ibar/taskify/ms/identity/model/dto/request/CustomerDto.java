package az.ibar.taskify.ms.identity.model.dto.request;

import az.ibar.taskify.lib.common.validation.constraint.AzPhone;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CustomerDto {

    @NotBlank(message = "validation.notBlank.organizationName")
    private String organizationName;

    @AzPhone
    private String phoneNumber;

    @NotBlank(message = "validation.notBlank.address")
    private String address;

}
