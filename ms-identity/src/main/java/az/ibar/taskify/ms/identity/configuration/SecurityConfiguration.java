package az.ibar.taskify.ms.identity.configuration;

import az.ibar.taskify.lib.common.component.MessageResolver;
import az.ibar.taskify.lib.common.component.security.AccessDeniedHandlerImpl;
import az.ibar.taskify.lib.common.component.security.AuthenticationEntryPointImpl;
import az.ibar.taskify.lib.common.component.security.JwtConfigurer;
import az.ibar.taskify.lib.common.component.security.TokenProvider;
import az.ibar.taskify.ms.identity.component.security.AuthenticationProviderImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.CorsFilter;


@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Import({TokenProvider.class, AccessDeniedHandlerImpl.class, AuthenticationEntryPointImpl.class})
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final String[] SWAGGER_PATHS = new String[]{
            "/v2/api-docs",
            "/v2/swagger.json",
            "/webjars/**",
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/configuration/ui",
            "/configuration/security"
    };

    private final CorsFilter corsFilter;
    private final TokenProvider tokenProvider;
    private final MessageResolver messageResolver;
    private final AccessDeniedHandlerImpl accessDeniedHandler;
    private final AuthenticationProviderImpl authenticationProvider;
    private final AuthenticationEntryPointImpl authenticationEntryPoint;

    @Override
    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**")
                .antMatchers(SWAGGER_PATHS)
                .antMatchers("/auth/login")
                .antMatchers("/auth/logout")
                .antMatchers("/auth/refresh")
                .antMatchers("/actuator/health");

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider);
    }

    @Override
    @SuppressWarnings("MethodLength")
    protected void configure(HttpSecurity http) throws Exception {
        http
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/auth/logout"))
                .and()
                .csrf()
                .disable()
                .exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler)
                .authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .headers()
                .contentSecurityPolicy("default-src 'self'; frame-src 'self' data:; script-src 'self' " +
                        "'unsafe-inline' 'unsafe-eval' https://storage.googleapis.com; style-src 'self' " +
                        "'unsafe-inline'; img-src 'self' data:; font-src 'self' data:")
                .and()
                .referrerPolicy(ReferrerPolicyHeaderWriter.ReferrerPolicy.STRICT_ORIGIN_WHEN_CROSS_ORIGIN)
                .and()
                .featurePolicy("geolocation 'none'; midi 'none'; sync-xhr 'none'; microphone 'none'; " +
                        "camera 'none'; magnetometer 'none'; gyroscope 'none'; speaker 'none'; " +
                        "fullscreen 'self'; payment 'none'")
                .and()
                .frameOptions()
                .deny()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/auth/login").permitAll()
                .antMatchers("/auth/logout").permitAll()
                .antMatchers("/auth/refresh").permitAll()
                .antMatchers("/registration/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)
                .apply(securityConfigurerAdapter());
    }

    private JwtConfigurer securityConfigurerAdapter() {
        return new JwtConfigurer(tokenProvider, messageResolver);
    }
}
