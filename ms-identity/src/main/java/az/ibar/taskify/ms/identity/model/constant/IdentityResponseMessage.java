package az.ibar.taskify.ms.identity.model.constant;

import az.ibar.taskify.lib.common.model.constant.ResponseMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IdentityResponseMessage implements ResponseMessage {

    EMAIL_ALREADY_EXIST("error.emailAlreadyExist"),
    INVALID_USER_CREDENTIALS("error.invalidCredentials"),
    INVALID_ACCESS_TOKEN("error.invalidAccessToken"),
    INVALID_REFRESH_TOKEN("error.invalidRefreshToken"),
    USER_NOT_FOUND("error.userNotFound");

    private final String messageKey;

}
