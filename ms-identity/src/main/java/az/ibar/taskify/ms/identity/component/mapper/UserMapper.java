package az.ibar.taskify.ms.identity.component.mapper;

import az.ibar.taskify.lib.common.model.CustomUserPrincipal;
import az.ibar.taskify.ms.identity.component.security.PasswordHelper;
import az.ibar.taskify.ms.identity.model.dto.request.UserDto;
import az.ibar.taskify.ms.identity.model.dto.request.UserRegistrationDto;
import az.ibar.taskify.ms.identity.model.dto.response.UserInfoDto;
import az.ibar.taskify.ms.identity.model.entity.Customer;
import az.ibar.taskify.ms.identity.model.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;

import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class UserMapper {

    private static final String CUSTOMER = "customer";

    protected PasswordHelper passwordHelper;

    @Autowired
    public void setPasswordHelper(PasswordHelper passwordHelper) {
        this.passwordHelper = passwordHelper;
    }

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = CUSTOMER, source = CUSTOMER)
    @Mapping(target = "password", source = "userDto.password", qualifiedByName = "encodePassword")
    public abstract User dto2Entity(UserDto userDto, Customer customer);

    public CustomUserPrincipal entity2Principal(User user, Set<GrantedAuthority> authorities) {
        CustomUserPrincipal principal = new CustomUserPrincipal(
                user.getEmail(),
                user.getPassword(),
                authorities
        );
        principal.setId(user.getId());
        principal.setCustomerId(user.getCustomer().getId());
        principal.setName(user.getName());
        principal.setSurname(user.getSurname());
        return principal;
    }

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = CUSTOMER, source = CUSTOMER)
    @Mapping(target = "password", source = "password", qualifiedByName = "encodePassword")
    public abstract User userRegistrationDto2Entity(UserRegistrationDto userRegistrationDto,
                                                    Customer customer, String password);

    @Named("encodePassword")
    protected String encodePassword(String rawPassword) {
        return passwordHelper.encode(rawPassword);
    }

    public abstract UserInfoDto entity2InfoDto(User user);
}
