package az.ibar.taskify.ms.identity.model.dto.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class UserRegistrationDto {

    @NotBlank(message = "validation.notBlank.name")
    private String name;

    @NotBlank(message = "validation.notBlank.surname")
    private String surname;

    @Email(message = "validation.email")
    private String email;

}
