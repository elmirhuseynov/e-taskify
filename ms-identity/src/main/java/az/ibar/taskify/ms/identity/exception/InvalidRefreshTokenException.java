package az.ibar.taskify.ms.identity.exception;

import az.ibar.taskify.lib.common.exception.BadRequestException;
import az.ibar.taskify.ms.identity.model.constant.IdentityResponseMessage;

public class InvalidRefreshTokenException extends BadRequestException {

    private static final long serialVersionUID = 4243232L;

    public InvalidRefreshTokenException() {
        super(IdentityResponseMessage.INVALID_REFRESH_TOKEN);
    }

}
