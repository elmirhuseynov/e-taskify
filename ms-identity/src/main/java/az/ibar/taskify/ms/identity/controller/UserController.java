package az.ibar.taskify.ms.identity.controller;

import az.ibar.taskify.ms.identity.model.dto.request.UserRegistrationDto;
import az.ibar.taskify.ms.identity.model.dto.response.UserInfoDto;
import az.ibar.taskify.ms.identity.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @PostMapping("/registration")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void registerUser(@Valid @RequestBody UserRegistrationDto userRegistrationDto) {
        userService.registerUser(userRegistrationDto);
    }

    @GetMapping("/info")
    public UserInfoDto getUserInfo(@NotNull @RequestParam Long userId) {
        return userService.getUserInfo(userId);
    }

}
