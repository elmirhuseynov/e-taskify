package az.ibar.taskify.ms.identity.service;

import az.ibar.taskify.lib.common.component.security.TokenProvider;
import az.ibar.taskify.ms.identity.component.mapper.TokenMapper;
import az.ibar.taskify.ms.identity.component.security.TokenHelper;
import az.ibar.taskify.ms.identity.exception.InvalidAccessTokenException;
import az.ibar.taskify.ms.identity.exception.InvalidRefreshTokenException;
import az.ibar.taskify.ms.identity.model.dto.request.LoginDto;
import az.ibar.taskify.ms.identity.model.dto.request.RefreshDto;
import az.ibar.taskify.ms.identity.model.dto.response.TokenDto;
import az.ibar.taskify.ms.identity.model.hash.Token;
import az.ibar.taskify.ms.identity.repository.redis.TokenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final TokenMapper tokenMapper;
    private final TokenHelper tokenHelper;
    private final TokenProvider tokenProvider;
    private final TokenRepository tokenRepository;
    private final AuthenticationManager authenticationManager;

    public TokenDto login(LoginDto loginDto) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginDto.getEmail(), loginDto.getPassword());
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        Token token = tokenHelper.createToken(authentication);
        return tokenMapper.hash2Dto(tokenRepository.save(token));
    }

    public void logout(String authorizationHeader) {
        String accessToken = tokenProvider.extractToken(authorizationHeader)
                .orElseThrow(InvalidAccessTokenException::new);
        Authentication authentication = tokenProvider.parseAuthentication(accessToken);
        tokenRepository.deleteById(authentication.getName());
    }

    public TokenDto refresh(RefreshDto refreshDto) {
        String refreshToken = refreshDto.getRefreshToken();
        Authentication authentication = tokenProvider.parseAuthentication(refreshToken);

        Optional<Token> tokenOptional = tokenRepository.findById(authentication.getName());
        tokenOptional.map(Token::getRefreshToken)
                .filter(refreshToken::equals)
                .orElseThrow(InvalidRefreshTokenException::new);

        Token token = tokenHelper.createToken(authentication);
        return tokenMapper.hash2Dto(tokenRepository.save(token));
    }
}
