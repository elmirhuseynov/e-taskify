package az.ibar.taskify.ms.identity.controller;

import az.ibar.taskify.ms.identity.model.dto.request.LoginDto;
import az.ibar.taskify.ms.identity.model.dto.request.RefreshDto;
import az.ibar.taskify.ms.identity.model.dto.response.TokenDto;
import az.ibar.taskify.ms.identity.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthController {

    private final AuthService authService;

    @PostMapping("/login")
    public TokenDto login(@Valid @RequestBody LoginDto loginDto) {
        return authService.login(loginDto);
    }

    @GetMapping("/logout")
    public void logout(@NotBlank(message = "validation.notBlank.authorizationHeader")
                       @RequestHeader("Authorization") String authorizationHeader) {
        authService.logout(authorizationHeader);
    }

    @PostMapping("/refresh")
    public TokenDto refresh(@Valid @RequestBody RefreshDto refreshDto) {
        return authService.refresh(refreshDto);
    }

}
