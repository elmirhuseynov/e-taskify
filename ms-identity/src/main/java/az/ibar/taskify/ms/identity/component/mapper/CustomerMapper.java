package az.ibar.taskify.ms.identity.component.mapper;

import az.ibar.taskify.ms.identity.model.dto.request.CustomerDto;
import az.ibar.taskify.ms.identity.model.entity.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    Customer dto2Entity(CustomerDto customerDto);

}
