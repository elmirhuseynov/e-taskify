package az.ibar.taskify.ms.identity.exception;


import az.ibar.taskify.lib.common.exception.BadRequestException;
import az.ibar.taskify.ms.identity.model.constant.IdentityResponseMessage;

public class InvalidCredentialsException extends BadRequestException {

    private static final long serialVersionUID = 1L;

    public InvalidCredentialsException() {
        super(IdentityResponseMessage.INVALID_USER_CREDENTIALS);
    }
}
