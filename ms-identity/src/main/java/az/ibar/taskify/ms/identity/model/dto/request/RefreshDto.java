package az.ibar.taskify.ms.identity.model.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RefreshDto {

    @NotBlank(message = "validation.notBlank.refreshToken")
    private String refreshToken;

}
