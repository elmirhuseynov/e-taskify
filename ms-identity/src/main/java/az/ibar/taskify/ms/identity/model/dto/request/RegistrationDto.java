package az.ibar.taskify.ms.identity.model.dto.request;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
public class RegistrationDto {

    @Valid
    @NotNull
    private CustomerDto customer;

    @Valid
    @NotNull
    private UserDto user;

}
