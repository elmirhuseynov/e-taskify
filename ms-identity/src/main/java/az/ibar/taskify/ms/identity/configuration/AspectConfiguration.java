//package az.ibar.taskify.ms.identity.configuration;
//
//import az.ibar.taskify.ms.identity.aspect.LoggingAspect;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.EnableAspectJAutoProxy;
//import org.springframework.core.env.Environment;
//
//@Configuration
//@EnableAspectJAutoProxy
//public class AspectConfiguration {
//
//    @Bean
//    public LoggingAspect loggingAspect(Environment env) {
//        return new LoggingAspect(env);
//    }
//
//}
