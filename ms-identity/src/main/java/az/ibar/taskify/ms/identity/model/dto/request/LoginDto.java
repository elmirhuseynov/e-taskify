package az.ibar.taskify.ms.identity.model.dto.request;

import lombok.Data;
import lombok.ToString;

@Data
public class LoginDto {

    private String email;

    @ToString.Exclude
    private String password;

}
