package az.ibar.taskify.ms.identity.service;

import az.ibar.taskify.ms.identity.component.mapper.CustomerMapper;
import az.ibar.taskify.ms.identity.component.mapper.UserMapper;
import az.ibar.taskify.ms.identity.model.dto.request.CustomerDto;
import az.ibar.taskify.ms.identity.model.dto.request.RegistrationDto;
import az.ibar.taskify.ms.identity.model.entity.Customer;
import az.ibar.taskify.ms.identity.model.entity.User;
import az.ibar.taskify.ms.identity.repository.CustomerRepository;
import az.ibar.taskify.ms.identity.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class RegistrationService {

    private final UserMapper userMapper;
    private final UserService userService;
    private final CustomerMapper customerMapper;
    private final UserRepository userRepository;
    private final UserRoleService userRoleService;
    private final CustomerRepository customerRepository;

    @Transactional
    public void signup(RegistrationDto registrationDto) {
        CustomerDto customerDto = registrationDto.getCustomer();
        Customer customer = customerRepository.save(customerMapper.dto2Entity(customerDto));

        User user = userMapper.dto2Entity(registrationDto.getUser(), customer);
        userService.throwExWhenEmailAlreadyExist(user.getEmail());
        user = userRepository.save(user);

        userRoleService.createAdminUserRole(user.getId());
    }


}
