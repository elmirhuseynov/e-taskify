package az.ibar.taskify.ms.identity.repository;

import az.ibar.taskify.ms.identity.model.constant.RoleName;
import az.ibar.taskify.ms.identity.model.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByRole(RoleName role);

    List<Role> findAllByRoleIn(List<RoleName> roles);

}
