package az.ibar.taskify.ms.identity.model.dto.response;

import lombok.Data;

@Data
public class UserInfoDto {

    private Long id;
    private String name;
    private String surname;
    private String email;

}
