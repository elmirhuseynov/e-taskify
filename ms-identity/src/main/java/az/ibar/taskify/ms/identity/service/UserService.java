package az.ibar.taskify.ms.identity.service;

import az.ibar.taskify.lib.common.model.event.MailEvent;
import az.ibar.taskify.lib.common.util.RandomUtils;
import az.ibar.taskify.lib.common.util.SecurityContextUtils;
import az.ibar.taskify.ms.identity.component.MessagePublisher;
import az.ibar.taskify.ms.identity.component.mapper.UserMapper;
import az.ibar.taskify.ms.identity.exception.EmailAlreadyExistException;
import az.ibar.taskify.ms.identity.exception.UserNotFoundException;
import az.ibar.taskify.ms.identity.model.dto.request.UserRegistrationDto;
import az.ibar.taskify.ms.identity.model.dto.response.UserInfoDto;
import az.ibar.taskify.ms.identity.model.entity.Customer;
import az.ibar.taskify.ms.identity.model.entity.User;
import az.ibar.taskify.ms.identity.repository.CustomerRepository;
import az.ibar.taskify.ms.identity.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static az.ibar.taskify.lib.common.model.constant.MessagingQueueConstants.EXCHANGE_MAIL;
import static az.ibar.taskify.lib.common.model.constant.MessagingQueueConstants.ROUTING_KEY_MAIL;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final UserRoleService userRoleService;
    private final MessagePublisher messagePublisher;
    private final CustomerRepository customerRepository;

    public Optional<User> findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void throwExWhenEmailAlreadyExist(String email) {
        findUserByEmail(email).ifPresent(user -> {
            throw new EmailAlreadyExistException(email);
        });
    }

    public void registerUser(UserRegistrationDto userRegistrationDto) {
        String password = RandomUtils.generateSecureRandom(8);

        Customer customer = customerRepository.findById(SecurityContextUtils.getCustomerId())
                .orElseThrow(); //TODO add exp message

        User user = userMapper.userRegistrationDto2Entity(userRegistrationDto, customer, password);
        throwExWhenEmailAlreadyExist(user.getEmail());

        user = userRepository.save(user);

        userRoleService.createSimpleUserRole(user.getId());

        MailEvent mailEvent = constructEmail(user.getEmail(), password);
        messagePublisher.publish(EXCHANGE_MAIL, ROUTING_KEY_MAIL, mailEvent);
    }

    public UserInfoDto getUserInfo(Long userId) {
        Long customerId = SecurityContextUtils.getCustomerId();
        return userRepository.findByIdAndCustomerId(userId, customerId)
                .map(userMapper::entity2InfoDto)
                .orElseThrow(UserNotFoundException::new);
    }

    public MailEvent constructEmail(String email, String password) { //TODO
        MailEvent mail = new MailEvent();
        mail.setFrom("e.taskify@gmail.com");
        mail.setTo(new String[]{email});
        mail.setSubject("New User account");
        mail.setText("Your Password is: " + password);
        return mail;
    }
}
