package az.ibar.taskify.ms.identity.service;

import az.ibar.taskify.ms.identity.model.constant.RoleName;
import az.ibar.taskify.ms.identity.model.entity.Role;
import az.ibar.taskify.ms.identity.model.entity.UserRole;
import az.ibar.taskify.ms.identity.repository.RoleRepository;
import az.ibar.taskify.ms.identity.repository.UserRoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserRoleService {

    private final RoleRepository roleRepository;
    private final UserRoleRepository userRoleRepository;

    public void createAdminUserRole(Long userId) {
        List<Role> adminRoles = roleRepository.findAllByRoleIn(
                List.of(RoleName.ROLE_ADMIN, RoleName.ROLE_USER)
        );

        List<UserRole> userRoles = adminRoles
                .stream()
                .map(r -> buildUserRole(userId, r.getId()))
                .collect(Collectors.toList());

        userRoleRepository.saveAll(userRoles);
    }

    public void createSimpleUserRole(Long userId) {
        Role role = roleRepository.findByRole(RoleName.ROLE_USER).orElseThrow();
        userRoleRepository.save(buildUserRole(userId, role.getId()));
    }

    private UserRole buildUserRole(Long userId, Long roleId) {
        return UserRole.builder()
                .userId(userId)
                .roleId(roleId)
                .build();
    }
}
