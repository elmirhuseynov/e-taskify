package az.ibar.taskify.ms.identity.exception;

import az.ibar.taskify.lib.common.exception.AlreadyExistException;
import az.ibar.taskify.ms.identity.model.constant.IdentityResponseMessage;

public class EmailAlreadyExistException extends AlreadyExistException {

    private static final long serialVersionUID = 4223443L;

    public EmailAlreadyExistException(String email) {
        super(IdentityResponseMessage.EMAIL_ALREADY_EXIST, new Object[]{email});
    }
}
