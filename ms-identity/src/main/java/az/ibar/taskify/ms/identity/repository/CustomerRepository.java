package az.ibar.taskify.ms.identity.repository;

import az.ibar.taskify.ms.identity.model.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
