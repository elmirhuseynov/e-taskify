package az.ibar.taskify.ms.identity.exception;

import az.ibar.taskify.lib.common.component.MessageResolver;
import az.ibar.taskify.lib.common.exception.GlobalExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class IdentityExceptionHandler extends GlobalExceptionHandler {

    public IdentityExceptionHandler(MessageResolver messageResolver) {
        super(messageResolver);
    }
}
