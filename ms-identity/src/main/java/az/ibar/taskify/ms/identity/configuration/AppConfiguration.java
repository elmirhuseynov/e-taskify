package az.ibar.taskify.ms.identity.configuration;

import az.ibar.taskify.lib.common.component.MessageResolver;
import az.ibar.taskify.lib.common.configuration.CommonConfiguration;
import az.ibar.taskify.lib.common.configuration.LocaleConfiguration;
import az.ibar.taskify.lib.common.configuration.WebConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@Import({CommonConfiguration.class, WebConfiguration.class, LocaleConfiguration.class, MessageResolver.class})
public class AppConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
