package az.ibar.taskify.ms.identity.model.constant;

public enum RoleName {

    ROLE_ADMIN,
    ROLE_USER;

}
