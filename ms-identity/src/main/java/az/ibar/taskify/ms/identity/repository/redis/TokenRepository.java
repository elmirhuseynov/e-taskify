package az.ibar.taskify.ms.identity.repository.redis;

import az.ibar.taskify.ms.identity.model.hash.Token;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends CrudRepository<Token, String> {
}
