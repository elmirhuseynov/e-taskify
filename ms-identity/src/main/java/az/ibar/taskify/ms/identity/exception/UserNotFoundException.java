package az.ibar.taskify.ms.identity.exception;

import az.ibar.taskify.lib.common.exception.NotFoundException;
import az.ibar.taskify.ms.identity.model.constant.IdentityResponseMessage;

public class UserNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 1L;

    public UserNotFoundException() {
        super(IdentityResponseMessage.USER_NOT_FOUND);
    }
}
