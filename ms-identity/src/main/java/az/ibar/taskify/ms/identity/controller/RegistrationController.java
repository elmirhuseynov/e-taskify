package az.ibar.taskify.ms.identity.controller;

import az.ibar.taskify.ms.identity.model.dto.request.RegistrationDto;
import az.ibar.taskify.ms.identity.service.RegistrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/registration")
public class RegistrationController {

    private final RegistrationService registrationService;

    @PostMapping("/signup")
    public void signup(@Valid @RequestBody RegistrationDto registrationDto) {
        registrationService.signup(registrationDto);
    }

}
