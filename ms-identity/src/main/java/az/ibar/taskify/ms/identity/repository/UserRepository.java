package az.ibar.taskify.ms.identity.repository;

import az.ibar.taskify.ms.identity.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    Optional<User> findByIdAndCustomerId(Long id, Long customerId);

}
