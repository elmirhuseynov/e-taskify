package az.ibar.taskify.ms.identity.component.mapper;

import az.ibar.taskify.ms.identity.model.dto.response.TokenDto;
import az.ibar.taskify.ms.identity.model.hash.Token;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TokenMapper {

    Token dto2Hash(TokenDto tokenDto);

    TokenDto hash2Dto(Token save);
}
