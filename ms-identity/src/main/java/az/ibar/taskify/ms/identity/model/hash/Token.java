package az.ibar.taskify.ms.identity.model.hash;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("AuthToken")
public class Token {

    @Id
    private String email;

    private String accessToken;
    private String refreshToken;

    @TimeToLive
    private long ttl;

}
