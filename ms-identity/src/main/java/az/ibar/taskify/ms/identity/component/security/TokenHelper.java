package az.ibar.taskify.ms.identity.component.security;

import az.ibar.taskify.lib.common.component.security.TokenProvider;
import az.ibar.taskify.lib.common.configuration.properties.SecurityProperties;
import az.ibar.taskify.ms.identity.model.hash.Token;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TokenHelper {

    private final TokenProvider tokenProvider;
    private final SecurityProperties securityProperties;

    public Token createToken(Authentication authentication) {
        return Token.builder()
                .accessToken(tokenProvider.createAccessToken(authentication))
                .refreshToken(tokenProvider.createRefreshToken(authentication))
                .email(authentication.getName())
                .ttl(securityProperties.getJwtProperties().getTimeToLiveInCache().toSeconds())
                .build();
    }

}
