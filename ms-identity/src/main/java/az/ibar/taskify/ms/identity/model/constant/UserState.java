package az.ibar.taskify.ms.identity.model.constant;

public enum UserState {

    ACTIVE,
    TEMPORARY_LOCKED,
    PERMANENTLY_LOCKED,
    DELETED

}
