package az.ibar.taskify.ms.identity.component.security;

import az.ibar.taskify.lib.common.model.CustomUserPrincipal;
import az.ibar.taskify.ms.identity.component.mapper.RoleMapper;
import az.ibar.taskify.ms.identity.component.mapper.UserMapper;
import az.ibar.taskify.ms.identity.exception.InvalidCredentialsException;
import az.ibar.taskify.ms.identity.model.entity.User;
import az.ibar.taskify.ms.identity.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class AuthenticationProviderImpl implements AuthenticationProvider {

    private final UserMapper userMapper;
    private final RoleMapper roleMapper;
    private final UserService userService;
    private final PasswordHelper passwordHelper;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken authenticationToken = (UsernamePasswordAuthenticationToken) authentication;
        String email = (String) authenticationToken.getPrincipal();
        String rawPassword = (String) authenticationToken.getCredentials();

        User user = userService.findUserByEmail(email).orElseThrow(InvalidCredentialsException::new);
        passwordHelper.validatePassword(rawPassword, user.getPassword());
        return buildAuthentication(user);
    }

    protected Authentication buildAuthentication(User user) {
        var authorities = roleMapper.mapRolesToGrantedAuthorities(user.getRoles());
        CustomUserPrincipal principal = userMapper.entity2Principal(user, authorities);
        return new UsernamePasswordAuthenticationToken(principal, user.getPassword(), authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
