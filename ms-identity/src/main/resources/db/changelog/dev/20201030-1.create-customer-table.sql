create table customer
(
    id                bigint       not null AUTO_INCREMENT,
    organization_name varchar(255) not null,
    phone_number      varchar(64)  not null,
    address           varchar(500) not null,
    created_date      timestamp             default CURRENT_TIMESTAMP,
    state             varchar(100) not null default 'ACTIVE',
    PRIMARY KEY (id)
);

create table user
(
    id                 bigint       not null AUTO_INCREMENT,
    customer_id        bigint       not null,
    name               varchar(100) not null,
    surname            varchar(100) not null,
    email              varchar(256) not null unique,
    password           varchar(512) not null,
    state              varchar(100) not null default 'ACTIVE',
    created_by         varchar(150) not null,
    created_date       timestamp    not null default CURRENT_TIMESTAMP,
    last_modified_by   varchar(150),
    last_modified_date timestamp    null     default null on update CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    CONSTRAINT user_customer_id_fk FOREIGN KEY (customer_id) REFERENCES customer (id)
);

create table role
(
    id           bigint      not null AUTO_INCREMENT,
    role         varchar(50) not null unique,
    description  varchar(150),
    created_date timestamp   not null default CURRENT_TIMESTAMP,
    active       boolean              default true,
    PRIMARY KEY (id)
);

create table user_role
(
    id           bigint    not null AUTO_INCREMENT,
    user_id      bigint    not null,
    role_id      bigint    not null,
    created_date timestamp not null default CURRENT_TIMESTAMP,
    active       boolean            default true,
    PRIMARY KEY (id),
    CONSTRAINT user_role_user_id_fk FOREIGN KEY (user_id) REFERENCES user (id),
    CONSTRAINT user_role_role_id_fk FOREIGN KEY (role_id) REFERENCES role (id)
);