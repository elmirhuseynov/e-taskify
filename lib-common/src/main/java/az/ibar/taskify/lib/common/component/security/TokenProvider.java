package az.ibar.taskify.lib.common.component.security;

import az.ibar.taskify.lib.common.configuration.properties.SecurityProperties;
import az.ibar.taskify.lib.common.exception.InvalidTokenException;
import az.ibar.taskify.lib.common.exception.TokenExpiredException;
import az.ibar.taskify.lib.common.model.CustomUserPrincipal;
import az.ibar.taskify.lib.common.model.constant.JwtConstants;
import az.ibar.taskify.lib.common.model.constant.TokenType;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.security.Key;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

import static az.ibar.taskify.lib.common.model.constant.TokenKey.AUTHORITIES;
import static az.ibar.taskify.lib.common.model.constant.TokenKey.CUSTOMER_ID;
import static az.ibar.taskify.lib.common.model.constant.TokenKey.ID;
import static az.ibar.taskify.lib.common.model.constant.TokenKey.NAME;
import static az.ibar.taskify.lib.common.model.constant.TokenKey.SURNAME;
import static az.ibar.taskify.lib.common.model.constant.TokenKey.TOKEN_TYPE;

@Slf4j
@Component
@RequiredArgsConstructor
public class TokenProvider {

    private Key key;
    private long accessTokenValidityInMs;
    private long refreshTokenValidityInMs;

    private final SecurityProperties securityProperties;

    @PostConstruct
    public void init() {
        var jwtProperties = securityProperties.getJwtProperties();
        byte[] keyBytes = Decoders.BASE64.decode(jwtProperties.getBase64Secret());
        this.key = Keys.hmacShaKeyFor(keyBytes);
        this.accessTokenValidityInMs = jwtProperties.getAccessTokenValidity().toMillis();
        this.refreshTokenValidityInMs = jwtProperties.getRefreshTokenValidity().toMillis();
    }


    public String createToken(Authentication authentication, TokenType tokenType) {
        String authorities = getAuthorities(authentication.getAuthorities());
        CustomUserPrincipal customUserPrincipal = (CustomUserPrincipal) authentication.getPrincipal();

        long tokenValidityInMs = tokenType == TokenType.ACCESS ? accessTokenValidityInMs : refreshTokenValidityInMs;

        Date validity = new Date(new Date().getTime() + tokenValidityInMs);

        return Jwts.builder()
                .setSubject(authentication.getName())
                .claim(AUTHORITIES, authorities)
                .claim(TOKEN_TYPE, tokenType)
                .claim(ID, customUserPrincipal.getId())
                .claim(CUSTOMER_ID, customUserPrincipal.getCustomerId())
                .claim(NAME, customUserPrincipal.getName())
                .claim(SURNAME, customUserPrincipal.getSurname())
                .signWith(key, SignatureAlgorithm.HS512)
                .setIssuedAt(new Date())
                .setExpiration(validity)
                .compact();
    }

    public String createAccessToken(Authentication authentication) {
        return createToken(authentication, TokenType.ACCESS);
    }

    public String createRefreshToken(Authentication authentication) {
        return createToken(authentication, TokenType.REFRESH);
    }

    public CustomUserPrincipal getPrincipal(Claims claims, Collection<? extends GrantedAuthority> authorities) {
        String login = claims.getSubject();
        Long id = claims.get(ID, Long.class);
        Long customerId = claims.get(CUSTOMER_ID, Long.class);
        String name = claims.get(NAME, String.class);
        String surname = claims.get(SURNAME, String.class);

        CustomUserPrincipal userPrincipal = new CustomUserPrincipal(login, "", authorities);
        userPrincipal.setId(id);
        userPrincipal.setCustomerId(customerId);
        userPrincipal.setName(name);
        userPrincipal.setSurname(surname);
        return userPrincipal;
    }

    public Authentication parseAuthentication(String token) {
        Claims claims = validateAndExtractClaim(token);
        var authorities = getGrantedAuthority((String) claims.get(AUTHORITIES));
        User principal = getPrincipal(claims, authorities);
        return new UsernamePasswordAuthenticationToken(principal, token, authorities);
    }

    @SuppressWarnings({"PMD.PreserveStackTrace", "PMD.AvoidCatchingGenericException"})
    public Claims validateAndExtractClaim(String token) {
        try {
            return extractClaim(token);
        } catch (ExpiredJwtException ignored) {
            throw new TokenExpiredException();
        } catch (Exception ignored) {
            throw new InvalidTokenException();
        }
    }

    public void validateToken(String token) {
        validateAndExtractClaim(token);
    }

    public Claims extractClaim(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String getAuthorities(Collection<? extends GrantedAuthority> grantedAuthorities) {
        if (grantedAuthorities == null) {
            return null;
        }

        return grantedAuthorities.stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(JwtConstants.TOKEN_CLAIM_DELIMITER));
    }

    public Collection<? extends GrantedAuthority> getGrantedAuthority(String auth) {
        if (StringUtils.isEmpty(auth)) {
            return AuthorityUtils.NO_AUTHORITIES;
        }

        return Arrays.stream(auth.split(JwtConstants.TOKEN_CLAIM_DELIMITER))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    public Optional<String> extractToken(String authorizationHeader) {
        return tokenFormatIsValid(authorizationHeader)
                ? Optional.of(authorizationHeader.replace(JwtConstants.TOKEN_PREFIX, "")) :
                Optional.empty();
    }

    public Optional<String> extractToken(HttpServletRequest request) {
        String authorizationHeader = request.getHeader(JwtConstants.AUTHORIZATION_HEADER);
        return extractToken(authorizationHeader);
    }

    public boolean tokenFormatIsValid(String authorizationHeader) {
        return StringUtils.hasText(authorizationHeader)
                && authorizationHeader.startsWith(JwtConstants.TOKEN_PREFIX)
                && authorizationHeader.length() > JwtConstants.TOKEN_START_INDEX;
    }

}
