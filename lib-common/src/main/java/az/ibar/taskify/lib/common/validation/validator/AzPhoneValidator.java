package az.ibar.taskify.lib.common.validation.validator;

import az.ibar.taskify.lib.common.util.ValidationUtils;
import az.ibar.taskify.lib.common.validation.constraint.AzPhone;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AzPhoneValidator implements ConstraintValidator<AzPhone, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return ValidationUtils.isValidAzPhone(value);
    }

}
