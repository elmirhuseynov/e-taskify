package az.ibar.taskify.lib.common.exception;

import az.ibar.taskify.lib.common.component.MessageResolver;
import az.ibar.taskify.lib.common.model.ValidationError;
import az.ibar.taskify.lib.common.model.constant.CommonResponseMessage;
import az.ibar.taskify.lib.common.model.constant.ResponseMessage;
import az.ibar.taskify.lib.common.util.RequestUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static az.ibar.taskify.lib.common.model.constant.CommonResponseMessage.ARGUMENT_VALIDATION_FAILED;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.ERROR;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.ERRORS;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.MESSAGE;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.PATH;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.STATUS;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler extends DefaultErrorAttributes {

    private final MessageResolver messageResolver;

    @ExceptionHandler(BaseException.class)
    public ResponseEntity<Map<String, Object>> handleBaseExceptions(BaseException ex,
                                                                    WebRequest webRequest) {
        return ofType(ex, webRequest);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Map<String, Object>> handleInternalServerErrors(Exception ex,
                                                                          WebRequest request) {
        log.error("Unexpected internal server error occurred", ex);
        return ofType(request, HttpStatus.INTERNAL_SERVER_ERROR, CommonResponseMessage.INTERNAL_ERROR);
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<Map<String, Object>> handleAuthenticationException(AuthenticationException ex,
                                                                             WebRequest request) {
        log.error("Authentication failed. 401 - UNAUTHORIZED. msg: {}", ex.getMessage());
        return ofType(request, HttpStatus.UNAUTHORIZED, CommonResponseMessage.UNAUTHORIZED);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Map<String, Object>> handleAccessDeniedException(AccessDeniedException ex,
                                                                           WebRequest request) {
        log.trace("Access is denied", ex);
        return ofType(request, HttpStatus.FORBIDDEN, CommonResponseMessage.FORBIDDEN);

    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Map<String, Object>> handleHttpMessageNotReadableEx(HttpMessageNotReadableException ex,
                                                                              WebRequest request) {
        log.error("Http message not readable", ex);
        return ofType(request, HttpStatus.BAD_REQUEST, CommonResponseMessage.HTTP_MESSAGE_NOT_READABLE);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<Map<String, Object>> handleHttpMethodNotSupportedEx(HttpRequestMethodNotSupportedException ex,
                                                                              WebRequest request) {
        log.trace("Http request method not supported", ex);
        return ofType(request, HttpStatus.BAD_REQUEST, CommonResponseMessage.HTTP_METHOD_NOT_SUPPORTED);
    }

    @ExceptionHandler(MissingRequestHeaderException.class)
    public ResponseEntity<Map<String, Object>> handleMissingRequestHeaderEx(MissingRequestHeaderException ex,
                                                                            WebRequest request) {
        log.trace("Missing request header", ex);
        return ofType(request, HttpStatus.BAD_REQUEST, CommonResponseMessage.MISSING_REQUEST_HEADER);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<Map<String, Object>> handleConstraintViolationEx(ConstraintViolationException ex,
                                                                                 WebRequest request) {
        log.info("Constraints violated {}", ex.getMessage());
        return ofType(request, HttpStatus.BAD_REQUEST, getConstraintViolationExceptionMessage(ex));
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public final ResponseEntity<Map<String, Object>> handleMethodArgumentTypeMismatchException(
            MethodArgumentTypeMismatchException ex,
            WebRequest request) {
        log.info("Method  arguments are not valid {}", ex.getMessage());
        return ofType(request, HttpStatus.BAD_REQUEST, ex.getMessage()); //TODO
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public final ResponseEntity<Map<String, Object>> handleMethodArgumentNotValidException(
            MethodArgumentNotValidException ex,
            WebRequest request) {
        List<ValidationError> validationErrors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(err -> new ValidationError(err.getField(), messageResolver.resolve(err.getDefaultMessage())))
                .collect(Collectors.toList());

        return ofType(request, HttpStatus.BAD_REQUEST, ARGUMENT_VALIDATION_FAILED, validationErrors);
    }

    public void buildResponse(@NotNull BaseException ex, HttpServletRequest request, @NotNull ServletResponse response)
            throws IOException {
        buildResponse(handleBaseExceptions(ex, new ServletWebRequest(request)), response);
    }

    public void buildResponse(@NotNull ResponseEntity<Map<String, Object>> entity, @NotNull ServletResponse response)
            throws IOException {
        byte[] responseToSend = new ObjectMapper().writeValueAsString(entity.getBody()).getBytes();
        ((HttpServletResponse) response).setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        ((HttpServletResponse) response).setStatus(entity.getStatusCodeValue());
        response.getOutputStream().write(responseToSend);
    }

    protected ResponseEntity<Map<String, Object>> ofType(BaseException ex, WebRequest request) {
        ResponseMessage rs = ex.getResponseMessage();
        Object[] objects = ex.getMessageObjects();
        log.info("Handled exception(validation) occurred, key: {}, message: {}", rs.getMessageKey(), ex.getMessage());
        String message = messageResolver.resolve(rs.getMessageKey(), objects);
        return ofType(request, ex.getHttpStatus(), message, Collections.emptyList());
    }

    protected ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status, String message) {
        return ofType(request, status, message, Collections.emptyList());
    }

    protected ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status, ResponseMessage rs) {
        return ofType(request, status, rs, Collections.emptyList());
    }

    protected ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status, ResponseMessage rs,
                                                         List<?> validationErrors) {
        String message = messageResolver.resolve(rs.getMessageKey());
        return ofType(request, status, message, validationErrors);
    }

    protected ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status, String message,
                                                         List<?> validationErrors) {
        Map<String, Object> attributes = getErrorAttributes(request, ErrorAttributeOptions.defaults());
        attributes.put(STATUS, status.value());
        attributes.put(ERROR, status.getReasonPhrase());
        attributes.put(MESSAGE, message);
        attributes.put(ERRORS, validationErrors);
        attributes.put(PATH, RequestUtils.getRequestPath(((ServletWebRequest) request).getRequest()));
        return new ResponseEntity<>(attributes, status);
    }

    private String getConstraintViolationExceptionMessage(ConstraintViolationException ex) {
        return ex.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getMessage)
                .map(messageResolver::resolve)
                .findFirst()
                .orElse(null);
    }

}
