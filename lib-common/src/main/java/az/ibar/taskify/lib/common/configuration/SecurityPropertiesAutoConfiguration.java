package az.ibar.taskify.lib.common.configuration;

import az.ibar.taskify.lib.common.configuration.properties.SecurityProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties({SecurityProperties.class})
public class SecurityPropertiesAutoConfiguration {
}
