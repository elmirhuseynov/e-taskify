//package az.ibar.taskify.lib.common.model.response;
//
//import az.ibar.taskify.lib.common.model.TraceError;
//import az.ibar.taskify.lib.common.model.ViolationError;
//import com.fasterxml.jackson.annotation.JsonInclude;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//import lombok.NoArgsConstructor;
//
//import java.util.List;
//
//@Data
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
//@EqualsAndHashCode(callSuper = false)
//public class ErrorResponse extends Response {
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    private List<ViolationError> violationErrors;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    private TraceError traceError;
//
//
//}
