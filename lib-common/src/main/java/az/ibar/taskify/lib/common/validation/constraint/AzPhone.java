package az.ibar.taskify.lib.common.validation.constraint;

import az.ibar.taskify.lib.common.validation.validator.AzPhoneValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = AzPhoneValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE,
        ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AzPhone {

    /**
     * Message key in resource bundle properties.
     */
    String message() default "validation.invalidPhone";

    @SuppressWarnings("JavadocMethod")
    Class<?>[] groups() default {};

    @SuppressWarnings("JavadocMethod")
    Class<? extends Payload>[] payload() default {};

}
