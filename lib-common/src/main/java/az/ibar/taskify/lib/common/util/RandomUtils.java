package az.ibar.taskify.lib.common.util;

import lombok.experimental.UtilityClass;

import java.security.SecureRandom;

@UtilityClass
public class RandomUtils {

    private static final int DEFAULT_LENGTH = 10;

    public static String generateSecureRandom() {
        return generateSecureRandom(DEFAULT_LENGTH, "");
    }

    public static String generateSecureRandom(String prefix) {
        return generateSecureRandom(DEFAULT_LENGTH, prefix);
    }

    public static String generateSecureRandom(int length) {
        return generateSecureRandom(length, "");
    }

    public static String generateSecureRandom(int length, String prefix) {
        String str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom random = new SecureRandom();
        StringBuilder token = new StringBuilder(prefix);
        for (int i = 0; i < length - prefix.length(); i++) {
            token.append(str.charAt(random.nextInt(str.length())));
        }
        return token.toString();
    }
}
