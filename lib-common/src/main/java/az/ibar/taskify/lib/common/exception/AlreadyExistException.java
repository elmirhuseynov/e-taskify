package az.ibar.taskify.lib.common.exception;

import az.ibar.taskify.lib.common.model.constant.ResponseMessage;

public class AlreadyExistException extends BadRequestException {

    private static final long serialVersionUID = 422343L;

    public AlreadyExistException(ResponseMessage responseStatus) {
        super(responseStatus);
    }

    public AlreadyExistException(ResponseMessage responseStatus, String message) {
        super(responseStatus, message);
    }

    public AlreadyExistException(ResponseMessage responseStatus, Object[] objects) {
        super(responseStatus, objects);
    }

}
