package az.ibar.taskify.lib.common.model.constant;

import java.io.Serializable;

public interface ResponseMessage extends Serializable {

    /**
     * Get the key of the message property.
     *
     * @return the key of the message property.
     */
    String getMessageKey();

}
