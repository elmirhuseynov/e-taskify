package az.ibar.taskify.lib.common.model.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CommonResponseMessage implements ResponseMessage {

    OK("success.ok"),
    INTERNAL_ERROR("error.internal"),
    HTTP_MESSAGE_NOT_READABLE("error.httpMessageNotReadable"),
    ERROR_CODE("error.exceptionCode"),
    ARGUMENT_VALIDATION_FAILED("error.argumentValidationFailed"),
    FORBIDDEN("error.forbidden"),
    UNAUTHORIZED("error.unauthorized"),
    ACCESS_TOKEN_EXPIRED("error.accessTokenExpired"),
    INVALID_TOKEN("error.invalidToken"),
    HTTP_METHOD_NOT_SUPPORTED("error.httpMethodNotSupported"),
    MISSING_REQUEST_HEADER("error.missingRequestHeader");

    private final String messageKey;

}
