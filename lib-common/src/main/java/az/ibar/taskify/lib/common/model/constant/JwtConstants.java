package az.ibar.taskify.lib.common.model.constant;

public final class JwtConstants {

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_CLAIM_DELIMITER = ",";
    public static final int TOKEN_START_INDEX = 7;

    private JwtConstants() {
    }
}
