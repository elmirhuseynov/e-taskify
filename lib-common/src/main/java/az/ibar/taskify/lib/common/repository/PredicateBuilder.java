//package az.ibar.taskify.lib.common.repository;
//
//import javax.persistence.criteria.CriteriaBuilder;
//import javax.persistence.criteria.Predicate;
//import javax.persistence.criteria.Root;
//import java.util.Optional;
//
///**
// * A base class to get a UserSearchView's field defined of type T by retriever,
// * check it with validate method and, if valid, convert to a predicate
// * @param <T> type of the field to apply criteria to (e.g. String or Date)
// * @param <SV> type of the search query view to extract the value of criteria field from using retriever
// *            (e.g. UserSearchView)
// * @param <E> the type of JPA entity to apply filtering to (e.g. UserProfile)
// */
//public interface PredicateBuilder<T, SV, E> {
//
//    Optional<Predicate> apply(Root<E> r, CriteriaBuilder b, SV searchView);
//}
