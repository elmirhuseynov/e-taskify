package az.ibar.taskify.lib.common.validation;

public final class ValidationPattern {

    public static final String PHONE_994 = "^(\\+994|994)[0-9]{9}$";
    public static final String PHONE_0 = "^0([0-9]{9}$)";
    public static final String PHONE_SHORT = "^([0-9]{9}$)";
    public static final String PASSWORD = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,40})";

    private ValidationPattern() {
    }
}
