package az.ibar.taskify.lib.common.exception;

import az.ibar.taskify.lib.common.model.constant.CommonResponseMessage;

public class TokenExpiredException extends UnauthorizedException {

    private static final long serialVersionUID = 1L;

    public TokenExpiredException() {
        super(CommonResponseMessage.ACCESS_TOKEN_EXPIRED);
    }
}
