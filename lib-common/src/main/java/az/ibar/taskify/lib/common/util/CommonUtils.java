package az.ibar.taskify.lib.common.util;

import lombok.experimental.UtilityClass;

import java.util.Optional;

@UtilityClass
public class CommonUtils {

    public static String createFullName(String name, String surname) {
        return Optional.ofNullable(name)
                .orElse("XXX")
                .concat(" ")
                .concat(Optional.ofNullable(surname)
                        .orElse("XXX")
                );
    }

}
