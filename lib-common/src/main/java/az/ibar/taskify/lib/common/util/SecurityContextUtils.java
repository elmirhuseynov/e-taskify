package az.ibar.taskify.lib.common.util;

import az.ibar.taskify.lib.common.exception.BaseException;
import az.ibar.taskify.lib.common.model.CustomUserPrincipal;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;
import java.util.function.Supplier;

@UtilityClass
public class SecurityContextUtils {

    public SecurityContext getSecurityContext() {
        return SecurityContextHolder.getContext();
    }

    public Authentication getAuthentication() {
        return getSecurityContext().getAuthentication();
    }

    public Optional<String> getUserToken() {
        return Optional.ofNullable(getAuthentication())
                .filter(authentication -> authentication.getCredentials() instanceof String)
                .map(authentication -> (String) authentication.getCredentials());
    }

    public Optional<CustomUserPrincipal> getCurrentUserPrincipal() {
        return Optional.ofNullable(getAuthentication())
                .map(Authentication::getPrincipal)
                .filter(CustomUserPrincipal.class::isInstance)
                .map(CustomUserPrincipal.class::cast);
    }

    public String getUsername() {
        return getCurrentUserPrincipal()
                .map(CustomUserPrincipal::getUsername)
                .orElse(null);
    }

    public <T extends BaseException> String getUsername(Supplier<T> exceptionSupplier) {
        return Optional.ofNullable(getUsername())
                .orElseThrow(exceptionSupplier);
    }

    public String getFullName() {
        return getOptionalFullName()
                .orElse(null);
    }

    public Optional<String> getOptionalFullName() {
        return getCurrentUserPrincipal()
                .map(principal -> CommonUtils.createFullName(principal.getName(), principal.getSurname()));
    }

    public Long getUserId() {
        return getCurrentUserPrincipal()
                .map(CustomUserPrincipal::getId)
                .orElse(null);
    }

    public Long getCustomerId() {
        return getCurrentUserPrincipal()
                .map(CustomUserPrincipal::getCustomerId)
                .orElse(null);
    }

}
