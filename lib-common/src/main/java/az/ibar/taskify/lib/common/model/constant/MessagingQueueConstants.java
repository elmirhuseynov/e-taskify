package az.ibar.taskify.lib.common.model.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class MessagingQueueConstants {

    public static final String EXCHANGE_MAIL = "MQ.Exchange.Mail";
    public static final String QUEUE_MAIL = "MQ.Queue.Mail";
    public static final String ROUTING_KEY_MAIL = "MQ.RoutingKey.Mail";

}
