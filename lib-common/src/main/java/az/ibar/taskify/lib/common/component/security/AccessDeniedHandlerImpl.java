package az.ibar.taskify.lib.common.component.security;

import az.ibar.taskify.lib.common.exception.GlobalExceptionHandler;
import az.ibar.taskify.lib.common.util.RequestUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;


@Slf4j
@Component
@RequiredArgsConstructor
public class AccessDeniedHandlerImpl implements AccessDeniedHandler {

    private final GlobalExceptionHandler exceptionHandler;

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException ex)
            throws IOException {
        log.error("FORBIDDEN(403). path: {}, msg: {}", RequestUtils.getRequestPath(request), ex.getMessage());
        ResponseEntity<Map<String, Object>> responseEntity = exceptionHandler
                .handleAccessDeniedException(ex, (WebRequest) request);
        exceptionHandler.buildResponse(responseEntity, response);
    }

}
