package az.ibar.taskify.lib.common.model.event;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
public class MailEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    private String subject;
    private String from;
    private String[] to;
    private String[] cc;
    private String[] bcc;

    @ToString.Exclude
    private String text;

}
