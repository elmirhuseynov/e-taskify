package az.ibar.taskify.lib.common.exception;

import az.ibar.taskify.lib.common.model.constant.ResponseMessage;
import org.springframework.http.HttpStatus;

public class UnauthorizedException extends BaseException {

    private static final long serialVersionUID = 1L;

    public UnauthorizedException(ResponseMessage responseStatus) {
        super(responseStatus, responseStatus.getMessageKey(), null, HttpStatus.UNAUTHORIZED);
    }

    public UnauthorizedException(ResponseMessage responseStatus, String message) {
        super(responseStatus, message, null, HttpStatus.UNAUTHORIZED);
    }

    public UnauthorizedException(ResponseMessage responseStatus, Object[] objects) {
        super(responseStatus, responseStatus.getMessageKey(), objects, HttpStatus.UNAUTHORIZED);
    }

}
