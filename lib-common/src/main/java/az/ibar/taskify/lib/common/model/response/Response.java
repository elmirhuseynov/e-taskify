//package az.ibar.taskify.lib.common.model.response;
//
//import az.ibar.taskify.lib.common.component.MessageResolver;
//import az.ibar.taskify.lib.common.model.constant.BaseResponseStatus;
//import az.ibar.taskify.lib.common.model.constant.IResponseStatus;
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.annotation.JsonInclude;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
//public class Response {
//
//    @JsonInclude
//    private Integer code;
//
//    @JsonInclude
//    private String userMessage;
//
//    private String technicalMessage;
//
//    @JsonIgnore
//    public static Response of(Integer code, String userMessage) {
//        return new Response(code, userMessage, null);
//    }
//
//    @JsonIgnore
//    public static Response of(Integer code, String userMessage, String technicalMessage) {
//        return new Response(code, userMessage, technicalMessage);
//    }
//
//    @JsonIgnore
//    public static Response of(IResponseStatus rs, MessageResolver mr) {
//        String userMessage;
//        String technicalMessage;
//        if (rs.nonVisible()) {
//            userMessage = mr.resolve(BaseResponseStatus.INTERNAL_ERROR.getMessageKey());
//            technicalMessage = mr.resolve(rs.getMessageKey());
//        } else {
//            userMessage = mr.resolve(rs.getMessageKey());
//            technicalMessage = null;
//        }
//        return of(rs.getCode(), userMessage, technicalMessage);
//    }
//
//}
