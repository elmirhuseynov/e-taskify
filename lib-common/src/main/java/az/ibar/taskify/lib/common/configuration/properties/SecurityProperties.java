package az.ibar.taskify.lib.common.configuration.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.cors.CorsConfiguration;

import java.time.Duration;

@Getter
@Setter
@ConfigurationProperties(prefix = "security")
public class SecurityProperties {

    private final JwtProperties jwtProperties = new JwtProperties();

    private final CorsConfiguration cors = new CorsConfiguration();

    @Getter
    @Setter
    public static class JwtProperties {

        private String base64Secret = "uY+1PvIcmK0RS32AfXdwUlHkUAOweflEdt5gajYYXQgr73HioR4+bs" +
                "J+I6Y9KgVFJWnRMRLJy1se3zgNjAJ3Nt9Pgj0jcqWP94yTn/yEzAJkV94H3jUL85pRkDwyF6vtQs" +
                "1DumF99ZWzuWlalyTUp2XXqQVY9okRCeD3nmPek/E=";

        private Duration accessTokenValidity = Duration.ofMinutes(30);

        private Duration refreshTokenValidity = Duration.ofHours(1);

        private Duration timeToLiveInCache = Duration.ofSeconds(
                Math.max(getRefreshTokenValidity().toSeconds(),
                        getAccessTokenValidity().toSeconds())
        );

    }

}
