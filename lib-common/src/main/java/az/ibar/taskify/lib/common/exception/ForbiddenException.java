package az.ibar.taskify.lib.common.exception;

import az.ibar.taskify.lib.common.model.constant.ResponseMessage;
import org.springframework.http.HttpStatus;

public class ForbiddenException extends BaseException {

    private static final long serialVersionUID = 4452L;

    public ForbiddenException(ResponseMessage responseStatus) {
        super(responseStatus, responseStatus.getMessageKey(), null, HttpStatus.FORBIDDEN);
    }

    public ForbiddenException(ResponseMessage responseStatus, String message) {
        super(responseStatus, message, null, HttpStatus.FORBIDDEN);
    }

    public ForbiddenException(ResponseMessage responseStatus, Object[] objects) {
        super(responseStatus, responseStatus.getMessageKey(), objects, HttpStatus.FORBIDDEN);
    }

}
