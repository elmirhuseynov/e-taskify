package az.ibar.taskify.lib.common.util;

import lombok.experimental.UtilityClass;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Optional;

@UtilityClass
public class RequestUtils {

    private static final String X_REAL_IP = "X-Real-IP";
    private static final String X_FORWARDED_FOR = "x-Forwarded-For";

    public static String getClientIp() {
        return Optional.ofNullable(getCurrentRequest())
                .map(RequestUtils::getClientIp)
                .orElse("unknown");
    }

    public static String getClientIp(HttpServletRequest httpServletRequest) {
        String remoteAddress = httpServletRequest.getHeader(X_REAL_IP);
        if (StringUtils.isEmpty(remoteAddress)) {
            remoteAddress = httpServletRequest.getHeader(X_FORWARDED_FOR);
        }
        if (StringUtils.isEmpty(remoteAddress)) {
            remoteAddress = httpServletRequest.getRemoteAddr();
        }
        return remoteAddress;
    }

    private static ServletRequestAttributes getServletRequestAttributes(RequestAttributes requestAttributes) {
        return (ServletRequestAttributes) requestAttributes;
    }

    private static HttpServletRequest getHttpServletRequest(ServletRequestAttributes servletRequestAttributes) {
        return servletRequestAttributes.getRequest();
    }

    public static String getRequestPath(HttpServletRequest request) {
        return Optional.ofNullable(request)
                .map(HttpServletRequest::getRequestURI)
                .orElse("unknown");
    }

    public static String getRequestPath() {
        return getRequestPath(getCurrentRequest());
    }

    public static HttpServletRequest getCurrentRequest() {
        return Optional.ofNullable(RequestContextHolder.getRequestAttributes())
                .map(RequestUtils::getServletRequestAttributes)
                .map(RequestUtils::getHttpServletRequest)
                .orElse(null);
    }

    public static String getLanguage() {
        return getLocale().getLanguage();
    }

    public static Locale getLocale() {
        return LocaleContextHolder.getLocale();
    }

}
