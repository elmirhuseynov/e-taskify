package az.ibar.taskify.lib.common.exception;

import az.ibar.taskify.lib.common.model.constant.ResponseMessage;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public abstract class BaseException extends RuntimeException {

    private static final long serialVersionUID = 4L;

    /**
     * Get the key of the message property from ResponseMessage.
     */
    private final ResponseMessage responseMessage;

    /**
     * For dynamic localization messages.
     */
    private final Object[] messageObjects;

    /**
     * Http status of exception.
     */
    private final HttpStatus httpStatus;

    protected BaseException(ResponseMessage responseMessage, HttpStatus httpStatus) {
        this(responseMessage, null, httpStatus);
    }

    protected BaseException(ResponseMessage responseMessage, Object[] messageObjects, HttpStatus httpStatus) {
        this(responseMessage, responseMessage.getMessageKey(), messageObjects, httpStatus);
    }

    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    protected BaseException(ResponseMessage responseMessage, String message,
                            Object[] messageObjects, HttpStatus httpStatus) {
        super(message);
        this.responseMessage = responseMessage;
        this.messageObjects = messageObjects;
        this.httpStatus = httpStatus;
    }

}
