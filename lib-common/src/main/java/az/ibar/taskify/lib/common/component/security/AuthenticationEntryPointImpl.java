package az.ibar.taskify.lib.common.component.security;

import az.ibar.taskify.lib.common.exception.GlobalExceptionHandler;
import az.ibar.taskify.lib.common.util.RequestUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint {

    private final GlobalExceptionHandler exceptionHandler;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException ex)
            throws IOException {
        log.error("UNAUTHORIZED(401). Path: {}, msg: {}", RequestUtils.getRequestPath(request), ex.getMessage());
        ResponseEntity<Map<String, Object>> responseEntity = exceptionHandler
                .handleAuthenticationException(ex, (WebRequest) request);
        exceptionHandler.buildResponse(responseEntity, response);
    }

}
