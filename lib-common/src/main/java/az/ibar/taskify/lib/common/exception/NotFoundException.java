package az.ibar.taskify.lib.common.exception;

import az.ibar.taskify.lib.common.model.constant.ResponseMessage;
import org.springframework.http.HttpStatus;

public class NotFoundException extends BaseException {

    private static final long serialVersionUID = 476542L;

    public NotFoundException(ResponseMessage responseStatus) {
        super(responseStatus, responseStatus.getMessageKey(), null, HttpStatus.NOT_FOUND);
    }

    public NotFoundException(ResponseMessage responseStatus, String message) {
        super(responseStatus, message, null, HttpStatus.NOT_FOUND);
    }

    public NotFoundException(ResponseMessage responseStatus, Object[] objects) {
        super(responseStatus, responseStatus.getMessageKey(), objects, HttpStatus.NOT_FOUND);
    }

}
