package az.ibar.taskify.lib.common.util;

import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import static az.ibar.taskify.lib.common.validation.ValidationPattern.PASSWORD;
import static az.ibar.taskify.lib.common.validation.ValidationPattern.PHONE_0;
import static az.ibar.taskify.lib.common.validation.ValidationPattern.PHONE_994;
import static az.ibar.taskify.lib.common.validation.ValidationPattern.PHONE_SHORT;

@UtilityClass
public class ValidationUtils {

    public boolean isValidPassword(String pwd) {
        if (pwd == null || pwd.isEmpty()) {
            return false;
        }
        return Pattern.compile(PASSWORD).matcher(pwd).matches();
    }

    public boolean isValidAzPhone(String phone) {
        if (phone == null) {
            return false;
        }
        List<String> telNumPatterns = new ArrayList<>(Arrays.asList(
                PHONE_994,
                PHONE_0,
                PHONE_SHORT)
        );
        return telNumPatterns.stream().anyMatch(phone::matches);
    }

    public boolean isPhone0(String phone) {
        /** for example 0705399387; 0556563645 etc..*/
        if (phone == null) {
            return false;
        }
        return phone.matches(PHONE_0);
    }

    public boolean isPhoneShort(String phone) {
        /** for example 705399387; 556563645 etc..*/
        if (phone == null) {
            return false;
        }
        return phone.matches(PHONE_SHORT);
    }

    public boolean isPhone994(String phone) {
        /** for example 994705399387; 994556563645 etc..*/
        if (phone == null) {
            return false;
        }
        return phone.matches(PHONE_994);
    }

}
