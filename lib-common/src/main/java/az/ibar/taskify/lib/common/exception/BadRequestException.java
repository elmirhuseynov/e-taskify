package az.ibar.taskify.lib.common.exception;

import az.ibar.taskify.lib.common.model.constant.ResponseMessage;
import org.springframework.http.HttpStatus;

public class BadRequestException extends BaseException {

    private static final long serialVersionUID = 1L;

    public BadRequestException(ResponseMessage responseStatus) {
        super(responseStatus, responseStatus.getMessageKey(), null, HttpStatus.BAD_REQUEST);
    }

    public BadRequestException(ResponseMessage responseStatus, String message) {
        super(responseStatus, message, null, HttpStatus.BAD_REQUEST);
    }

    public BadRequestException(ResponseMessage responseStatus, Object[] objects) {
        super(responseStatus, responseStatus.getMessageKey(), objects, HttpStatus.BAD_REQUEST);
    }

}
