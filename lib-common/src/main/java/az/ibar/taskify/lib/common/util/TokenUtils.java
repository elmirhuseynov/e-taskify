//package az.ibar.taskify.lib.common.util;
//
//import az.ibar.taskify.lib.common.exception.BaseException;
//import az.ibar.taskify.lib.common.model.constant.JwtConstants;
//import lombok.experimental.UtilityClass;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.AuthorityUtils;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.util.StringUtils;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.Arrays;
//import java.util.Collection;
//import java.util.function.Supplier;
//import java.util.stream.Collectors;
//
//
//@Slf4j
//@UtilityClass
//public final class TokenUtils {
//
//    public static String getAuthorities(Collection<? extends GrantedAuthority> grantedAuthorities) {
//        if (grantedAuthorities == null) return null;
//
//        return grantedAuthorities.stream()
//                .map(GrantedAuthority::getAuthority)
//                .collect(Collectors.joining(JwtConstants.TOKEN_CLAIM_DELIMITER));
//    }
//
//    public static Collection<? extends GrantedAuthority> getGrantedAuthority(String auth) {
//        if (StringUtils.isEmpty(auth)) return AuthorityUtils.NO_AUTHORITIES;
//
//        return Arrays.stream(auth.split(JwtConstants.TOKEN_CLAIM_DELIMITER))
//                .map(SimpleGrantedAuthority::new)
//                .collect(Collectors.toList());
//    }
//
//    public static <T extends BaseException> String extractToken(String authorizationHeader,
//                                                                Supplier<T> exceptionSupplier) {
//        String token = extractToken(authorizationHeader);
//        if (StringUtils.isEmpty(token)) {
//            throw exceptionSupplier.get();
//        } else {
//            return token;
//        }
//    }
//
//    public static String extractToken(String authorizationHeader) {
//        return tokenFormatIsValid(authorizationHeader) ?
//                authorizationHeader.replace(JwtConstants.TOKEN_PREFIX, "") : null;
//    }
//
//    public static String extractToken(HttpServletRequest request) {
//        String authorizationHeader = request.getHeader(JwtConstants.AUTHORIZATION_HEADER);
//        return extractToken(authorizationHeader);
//    }
//
//    public static <T extends BaseException> String extractToken(HttpServletRequest request,
//                                                                Supplier<T> exceptionSupplier) {
//        String authorizationHeader = request.getHeader(JwtConstants.AUTHORIZATION_HEADER);
//        return extractToken(authorizationHeader, exceptionSupplier);
//    }
//
//    public static boolean tokenFormatIsValid(String authorizationHeader) {
//        return StringUtils.hasText(authorizationHeader)
//                && authorizationHeader.startsWith(JwtConstants.TOKEN_PREFIX)
//                && authorizationHeader.length() > JwtConstants.TOKEN_START_INDEX;
//    }
//
//}
