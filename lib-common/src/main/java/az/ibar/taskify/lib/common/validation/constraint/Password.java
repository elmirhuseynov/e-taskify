package az.ibar.taskify.lib.common.validation.constraint;

import az.ibar.taskify.lib.common.validation.ValidationPattern;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Pattern(regexp = ValidationPattern.PASSWORD, message = "validation.invalidPassword")
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE,
        ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
public @interface Password {

    /**
     * Message key in resource bundle properties.
     */
    String message() default "validation.invalidPassword";

    @SuppressWarnings("JavadocMethod")
    Class<?>[] groups() default {};

    @SuppressWarnings("JavadocMethod")
    Class<? extends Payload>[] payload() default {};

}
