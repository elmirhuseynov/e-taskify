package az.ibar.taskify.lib.common.model.constant;

public final class TokenKey {

    public static final String AUTHORITIES = "authorities";
    public static final String TOKEN_TYPE = "token_type";
    public static final String ID = "id";
    public static final String CUSTOMER_ID = "customer_id";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";

    private TokenKey() {
    }

}
