package az.ibar.taskify.lib.common.component.security;

import az.ibar.taskify.lib.common.component.MessageResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@RequiredArgsConstructor
public class JwtConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    private final TokenProvider tokenProvider;
    private final MessageResolver messageResolver;

    @Override
    public void configure(HttpSecurity builder) {
        JwtFilter filter = new JwtFilter(tokenProvider, messageResolver);
        builder.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class);
    }
}
