package az.ibar.taskify.lib.common.exception;

import az.ibar.taskify.lib.common.model.constant.CommonResponseMessage;
import org.springframework.http.HttpStatus;

public class InternalServerException extends BaseException {

    private static final long serialVersionUID = 434L;

    public InternalServerException(String message) {
        super(CommonResponseMessage.INTERNAL_ERROR, message, null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public InternalServerException() {
        super(CommonResponseMessage.INTERNAL_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
