package az.ibar.taskify.lib.common.component.security;

import az.ibar.taskify.lib.common.component.MessageResolver;
import az.ibar.taskify.lib.common.exception.BaseException;
import az.ibar.taskify.lib.common.exception.InternalServerException;
import az.ibar.taskify.lib.common.util.RequestUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.ERROR;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.MESSAGE;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.PATH;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.STATUS;

@Slf4j
@RequiredArgsConstructor
public class JwtFilter extends GenericFilterBean {

    private final TokenProvider tokenProvider;
    private final MessageResolver messageResolver;

    @Override
    @SuppressWarnings({"PMD.AvoidCatchingGenericException"})
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        log.info("{}, is filtering...", RequestUtils.getRequestPath((HttpServletRequest) servletRequest));

        String jwt = tokenProvider.extractToken((HttpServletRequest) servletRequest)
                .orElse(null);
        if (StringUtils.hasText(jwt)) {
            try {
                Authentication authentication = tokenProvider.parseAuthentication(jwt);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            } catch (BaseException ex) {
                SecurityContextHolder.clearContext();
                log.warn("BaseException occurred. ResponseStatus: {}", ex.getResponseMessage());
                buildResponse(ex, (HttpServletRequest) servletRequest, servletResponse);
                return;
            } catch (Exception exception) {
                SecurityContextHolder.clearContext();
                log.error("Couldn't validate the token. Message: {}", exception.getMessage(), exception);
                buildResponse(new InternalServerException(exception.getMessage()),
                        (HttpServletRequest) servletRequest, servletResponse);
                return;
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    //TODO Inject Global Exception handler and use
    private void buildResponse(@NotNull BaseException ex,
                               @NotNull HttpServletRequest request,
                               @NotNull ServletResponse response)
            throws IOException {
        buildResponse(ofType(request, ex.getHttpStatus(),
                messageResolver.resolve(ex.getResponseMessage().getMessageKey())), response);
    }

    //TODO Inject Global Exception handler and use
    private void buildResponse(@NotNull ResponseEntity<Map<String, Object>> entity,
                               @NotNull ServletResponse response)
            throws IOException {
        byte[] responseToSend = new ObjectMapper().writeValueAsString(entity.getBody()).getBytes();
        ((HttpServletResponse) response).setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        ((HttpServletResponse) response).setStatus(entity.getStatusCodeValue());
        response.getOutputStream().write(responseToSend);
    }

    //TODO Inject Global Exception handler and use
    private ResponseEntity<Map<String, Object>> ofType(HttpServletRequest request, HttpStatus status, String message) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put(STATUS, status.value());
        attributes.put(ERROR, status.getReasonPhrase());
        attributes.put(MESSAGE, message);
        attributes.put(PATH, RequestUtils.getRequestPath(request));
        return new ResponseEntity<>(attributes, status);
    }

}
