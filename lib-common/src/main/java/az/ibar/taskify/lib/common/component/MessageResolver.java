package az.ibar.taskify.lib.common.component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Slf4j
@Component
@RequiredArgsConstructor
public class MessageResolver {

    private final MessageSource messageSource;

    public String resolve(String messageCode, Object[] arg, Locale locale) {
        return messageSource.getMessage(messageCode, arg, locale);
    }

    public String resolve(String messageCode, Object[] arg) {
        return resolve(messageCode, arg, LocaleContextHolder.getLocale());
    }

    public String resolve(String messageCode) {
        return resolve(messageCode, null);
    }


}
