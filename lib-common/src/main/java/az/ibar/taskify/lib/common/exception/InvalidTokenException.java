package az.ibar.taskify.lib.common.exception;

import az.ibar.taskify.lib.common.model.constant.CommonResponseMessage;

public class InvalidTokenException extends BadRequestException {

    private static final long serialVersionUID = 4232345L;

    public InvalidTokenException() {
        super(CommonResponseMessage.INVALID_TOKEN);
    }

}
