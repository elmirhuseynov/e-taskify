package az.ibar.taskify.ms.notification.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class MailConfiguration {

    @Value("${spring.mail.host}")
    private String host;

    @Value("${spring.mail.port}")
    private Integer port;

    @Value("${spring.mail.properties.mail.smtp.auth}")
    private Boolean isAuth;

    @Value("${spring.mail.properties.mail.smtp.starttls.enable}")
    private Boolean isStartTlsEnable;

    @Value("${spring.mail.properties.mail.smtp.connectiontimeout}")
    private Integer connectionTimeout;

    @Value("${spring.mail.properties.mail.smtp.timeout}")
    private Integer timeout;

    @Value("${spring.mail.properties.mail.smtp.writetimeout}")
    private Integer writeTimeout;

    @Bean
    public JavaMailSender javaMailSender() {
        final JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setUsername("e.taskify@gmail.com");
        mailSender.setPassword("eTaskify!@#");

        final Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.smtp.auth", isAuth);
        javaMailProperties.put("mail.smtp.starttls.enable", isStartTlsEnable);
        javaMailProperties.put("mail.transport.protocol", "smtp");

        javaMailProperties.put("mail.smtp.connectiontimeout", connectionTimeout);
        javaMailProperties.put("mail.smtp.timeout", timeout);
        javaMailProperties.put("mail.smtp.writetimeout", writeTimeout);

        mailSender.setJavaMailProperties(javaMailProperties);
        return mailSender;
    }

}
