package az.ibar.taskify.ms.notification.component.listener;

import az.ibar.taskify.lib.common.model.event.MailEvent;
import az.ibar.taskify.ms.notification.component.MailSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

import static az.ibar.taskify.lib.common.model.constant.MessagingQueueConstants.QUEUE_MAIL;

@Slf4j
@Component
@RequiredArgsConstructor
public class MailListener {

    private final MailSender mailSender;

    @RabbitListener(queues = QUEUE_MAIL)
    public void listenMailQueue(MailEvent mailEvent) throws MessagingException { //TODO
        log.info("mail queue listening. Mail: {}", mailEvent);
        mailSender.sendMail(mailEvent);
    }

}
