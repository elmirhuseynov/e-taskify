package az.ibar.taskify.ms.notification.component;

import az.ibar.taskify.lib.common.model.event.MailEvent;
import org.apache.commons.lang3.StringUtils;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class MailSender {

    private final JavaMailSender javaMailSender;

    public MailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendMail(MailEvent mail) throws MessagingException {
        final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        message.setFrom("e.taskify@gmail.com"); //TODO
        message.setTo(mail.getTo());

        String subject = mail.getSubject();
        if (StringUtils.isNotEmpty(subject)) {
            message.setSubject(mail.getSubject());
        }

        String[] cc = mail.getCc();
        if (cc != null && cc.length != 0) {
            message.setCc(cc);
        }

        String[] bcc = mail.getBcc();
        if (bcc != null && bcc.length != 0) {
            message.setBcc(bcc);
        }

        message.setText(mail.getText(), true);

        javaMailSender.send(mimeMessage);
    }

}
