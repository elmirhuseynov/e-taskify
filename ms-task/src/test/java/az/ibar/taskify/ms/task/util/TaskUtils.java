package az.ibar.taskify.ms.task.util;

import az.ibar.taskify.ms.task.model.constant.TaskStatus;
import az.ibar.taskify.ms.task.model.dto.request.ChangeTaskStatusDto;
import az.ibar.taskify.ms.task.model.dto.request.CreateTaskDto;
import az.ibar.taskify.ms.task.model.entity.Task;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static az.ibar.taskify.ms.task.util.CommonUtils.ID;

public final class TaskUtils {

    public static CreateTaskDto buildCreateTaskDto() {
        var createTaskDto = new CreateTaskDto();
        createTaskDto.setTitle("Title");
        createTaskDto.setDescription("Description");
        createTaskDto.setAssigneeId(ID);
        createTaskDto.setDeadline(LocalDate.MAX);
        return createTaskDto;
    }

    public static Task buildTask() {
        var createTaskDto = buildCreateTaskDto();
        var task = new Task();

        task.setId(ID);
        task.setAssigneeId(createTaskDto.getAssigneeId());
        task.setCustomerId(ID);
        task.setReporterId(ID);
        task.setDeadline(createTaskDto.getDeadline());
        task.setDescription(createTaskDto.getDescription());
        task.setTitle(createTaskDto.getTitle());
        task.setTaskStatus(TaskStatus.TODO);
        task.setCreatedBy("Elmir Huseyniv");
        task.setCreatedDate(LocalDateTime.now());
        return task;
    }

    public static ChangeTaskStatusDto buildChangeTaskStatusDto() {
        var changeTaskStatusDto = new ChangeTaskStatusDto();
        changeTaskStatusDto.setTaskId(ID);
        changeTaskStatusDto.setTaskStatus(TaskStatus.TODO);

        return changeTaskStatusDto;
    }

    private TaskUtils() {
    }
}
