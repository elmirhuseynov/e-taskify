package az.ibar.taskify.ms.task.service;

import az.ibar.taskify.ms.task.client.IdentityClient;
import az.ibar.taskify.ms.task.component.MessagePublisher;
import az.ibar.taskify.ms.task.component.generator.MailConstructor;
import az.ibar.taskify.ms.task.component.mapper.TaskMapper;
import az.ibar.taskify.ms.task.model.dto.request.CreateTaskDto;
import az.ibar.taskify.ms.task.model.entity.Task;
import az.ibar.taskify.ms.task.repository.TaskRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;

import static az.ibar.taskify.ms.task.util.TaskUtils.buildCreateTaskDto;
import static az.ibar.taskify.ms.task.util.TaskUtils.buildTask;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TaskServiceTest {

    @InjectMocks
    private TaskService taskService;

    @Mock
    private TaskMapper taskMapper;

    @Mock
    private MailConstructor mailConstructor;

    @Mock
    private IdentityClient identityClient;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private MessagePublisher messagePublisher;

    @Mock
    private HttpServletRequest servletRequest;

    @Test
    void testCreateTask() {
        //Arrange
        CreateTaskDto createTaskDto = buildCreateTaskDto();
        Task task = buildTask();
        when(taskMapper.dto2Entity(createTaskDto, null, null))
                .thenReturn(task); //TODO

        //Act
        taskService.createTask(createTaskDto);

        //Verify
        verify(taskRepository).save(task);
    }

}
