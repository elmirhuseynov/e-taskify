package az.ibar.taskify.ms.task.controller;

import az.ibar.taskify.lib.common.component.MessageResolver;
import az.ibar.taskify.lib.common.component.security.AccessDeniedHandlerImpl;
import az.ibar.taskify.lib.common.component.security.AuthenticationEntryPointImpl;
import az.ibar.taskify.lib.common.component.security.TokenProvider;
import az.ibar.taskify.ms.task.model.dto.request.ChangeTaskStatusDto;
import az.ibar.taskify.ms.task.model.dto.request.CreateTaskDto;
import az.ibar.taskify.ms.task.service.TaskNotFoundException;
import az.ibar.taskify.ms.task.service.TaskService;
import az.ibar.taskify.ms.task.service.TaskTransitionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.filter.CorsFilter;

import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.ERROR;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.PATH;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.STATUS;
import static az.ibar.taskify.lib.common.model.constant.HttpResponseConstants.TIMESTAMP;
import static az.ibar.taskify.ms.task.util.CommonUtils.ID;
import static az.ibar.taskify.ms.task.util.TaskUtils.buildChangeTaskStatusDto;
import static az.ibar.taskify.ms.task.util.TaskUtils.buildCreateTaskDto;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TaskController.class)
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SuppressWarnings("PMD.UnusedPrivateField")
public class TaskControllerTest {

    private static final String BASE_REQUEST_PATH = "/task";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TaskService taskService;

    @MockBean
    private TaskTransitionService taskTransitionService;

    @MockBean
    private CorsFilter corsFilter;

    @MockBean
    private TokenProvider tokenProvider;

    @MockBean
    private MessageResolver messageResolver;

    @MockBean
    private AccessDeniedHandlerImpl accessDeniedHandler;

    @MockBean
    private AuthenticationEntryPointImpl authenticationEntryPoint;

    @Test
    void givenCorrectParamWhenCreateTaskThenResponseIsOk() throws Exception {
        //Arrange
        CreateTaskDto createTaskDto = buildCreateTaskDto();
        doNothing().when(taskService).createTask(createTaskDto);

        mockMvc.perform(post(BASE_REQUEST_PATH)
                .content(objectToJson(createTaskDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());
    }

    @Test
    void givenCorrectParamWhenAssigneeThenResponseIsOk() throws Exception {
        //Arrange
        doNothing().when(taskService).assignTask(ID, ID);

        mockMvc.perform(put(BASE_REQUEST_PATH.concat("/assignee"))
                .param("taskId", String.valueOf(ID))
                .param("assigneeId", String.valueOf(ID))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());
    }

    @Test
    void givenCorrectParamWhenStatusChangingThenResponseIsOk() throws Exception {
        //Arrange
        ChangeTaskStatusDto changeTaskStatusDto = buildChangeTaskStatusDto();
        doNothing().when(taskTransitionService).changeTaskStatus(changeTaskStatusDto);

        mockMvc.perform(post(BASE_REQUEST_PATH.concat("/statusChanging"))
                .content(objectToJson(changeTaskStatusDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());
    }

    @Test
    void givenIncorrectIdWhenStatusChangingThenResponseIsNotFound() throws Exception {
        //Arrange
        String path = BASE_REQUEST_PATH.concat("/statusChanging");
        ChangeTaskStatusDto changeTaskStatusDto = buildChangeTaskStatusDto();
        doThrow(new TaskNotFoundException()).when(taskTransitionService).changeTaskStatus(changeTaskStatusDto);

        mockMvc.perform(post(path)
                .content(objectToJson(changeTaskStatusDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(STATUS, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(TIMESTAMP).isNotEmpty())
                .andExpect(jsonPath(ERROR, is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath(PATH, is(path)));
    }

    private String objectToJson(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }
}
