package az.ibar.taskify.ms.task.util;

public final class CommonUtils {

    public static final Long ID = -1L;
    public static final String EMAIL = "test@test.com";
    public static final String AUTHORIZATION_HEADER = "BEARER TOKEN";

    private CommonUtils() {
    }
}
