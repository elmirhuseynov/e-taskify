create table task
(
    id                 bigint       not null AUTO_INCREMENT,
    customer_id        bigint       not null,
    reporter_user_id   bigint       not null,
    assignee_user_id   bigint       not null,
    title              varchar(512) not null,
    description        varchar(2048),
    deadline           date         not null,
    status             varchar(100) not null default 'TODO',
    created_by         varchar(150) not null,
    created_date       timestamp    not null default CURRENT_TIMESTAMP,
    last_modified_by   varchar(150),
    last_modified_date timestamp    null     default null on update CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);