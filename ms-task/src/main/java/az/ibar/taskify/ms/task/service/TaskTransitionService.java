package az.ibar.taskify.ms.task.service;

import az.ibar.taskify.ms.task.component.mapper.TaskMapper;
import az.ibar.taskify.ms.task.component.transition.TaskTransition;
import az.ibar.taskify.ms.task.exception.TaskStatusChangingNotAllowedException;
import az.ibar.taskify.ms.task.model.constant.TaskStatus;
import az.ibar.taskify.ms.task.model.dto.request.ChangeTaskStatusDto;
import az.ibar.taskify.ms.task.model.entity.Task;
import az.ibar.taskify.ms.task.repository.TaskRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class TaskTransitionService {

    private final TaskMapper taskMapper;
    private final TaskRepository taskRepository;
    private Map<String, TaskTransition> taskTransitionMap;

    public TaskTransitionService(List<TaskTransition> taskTransitions,
                                 TaskRepository taskRepository,
                                 TaskMapper taskMapper) {
        initTransitions(taskTransitions);
        this.taskRepository = taskRepository;
        this.taskMapper = taskMapper;
    }


    private void initTransitions(List<TaskTransition> taskTransitions) {
        Map<String, TaskTransition> transitionHashMap = new HashMap<>();
        for (TaskTransition taskTransition : taskTransitions) {
            if (transitionHashMap.containsKey(taskTransition.getName())) {
                throw new IllegalStateException("Duplicate transition" + taskTransition.getName());
                //TODO add handled local exp
            }
            transitionHashMap.put(taskTransition.getName(), taskTransition);
        }
        taskTransitionMap = Collections.unmodifiableMap(transitionHashMap);
    }

    public List<String> getAllowedTransitions(Long id) {
        Task task = taskRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Unknown order: " + id));
        //TODO add handled locale exp
        return task.getTaskStatus().getTransitions();
    }

    public void transitionTask(Long id, String transitionName) {
        TaskTransition taskTransition = taskTransitionMap.get(transitionName);
        if (taskTransition == null) {
            throw new IllegalArgumentException("Unknown transition: " + transitionName);
            //TODO add locale exp
        }
        taskRepository.findById(id)
                .map(task -> {
                    checkAllowed(taskTransition, task.getTaskStatus());
                    taskTransition.applyProcessing(taskMapper.entity2Dto(task));
                    return updateTaskStatus(task, taskTransition.getStatus());
                }).orElseThrow(TaskNotFoundException::new);
    }

    private void checkAllowed(TaskTransition taskTransition, TaskStatus status) {
        Set<TaskStatus> allowedSourceStatuses = Stream.of(TaskStatus.values())
                .filter(s -> s.getTransitions().contains(taskTransition.getName()))
                .collect(Collectors.toSet());

        if (!allowedSourceStatuses.contains(status)) {
            log.info("The transition from the {} status to the {} status is not allowed!",
                    status, taskTransition.getName());
            throw new TaskStatusChangingNotAllowedException(new Object[]{status.name(), taskTransition.getName()});
        }
    }

    private Task updateTaskStatus(Task task, TaskStatus updatedStatus) {
        //TaskStatus existingStatus = task.getTaskStatus(); //TODO
        task.setTaskStatus(updatedStatus);

        return taskRepository.save(task);
    }

    public void changeTaskStatus(ChangeTaskStatusDto changeTaskStatusDto) {
        transitionTask(changeTaskStatusDto.getTaskId(), changeTaskStatusDto.getTaskStatus().name());
    }
}
