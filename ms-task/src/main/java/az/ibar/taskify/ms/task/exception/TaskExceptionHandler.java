package az.ibar.taskify.ms.task.exception;

import az.ibar.taskify.lib.common.component.MessageResolver;
import az.ibar.taskify.lib.common.exception.GlobalExceptionHandler;
import az.ibar.taskify.lib.common.model.constant.CommonResponseMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

@Slf4j
@RestControllerAdvice
public class TaskExceptionHandler extends GlobalExceptionHandler {

    private final ObjectMapper objectMapper;

    public TaskExceptionHandler(MessageResolver messageResolver,
                                ObjectMapper objectMapper) {
        super(messageResolver);
        this.objectMapper = objectMapper;
    }

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<Map<String, Object>> handleFeignExceptions(FeignException ex,
                                                                     WebRequest webRequest) {
        try {
            HttpStatus httpStatus = HttpStatus.valueOf(ex.status());
            Map<String, Object> objectMap = objectMapper.readValue(ex.contentUTF8(), Map.class);
            return new ResponseEntity<>(objectMap, httpStatus);
        } catch (JsonProcessingException exp) {
            log.error("JsonProcessingException occurred!", exp);
            return ofType(webRequest, HttpStatus.INTERNAL_SERVER_ERROR, CommonResponseMessage.INTERNAL_ERROR);
        }
    }
}
