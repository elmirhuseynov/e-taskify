package az.ibar.taskify.ms.task.repository;

import az.ibar.taskify.ms.task.model.entity.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TaskRepository extends PagingAndSortingRepository<Task, Long>, JpaSpecificationExecutor<Task> {

    Optional<Task> findByIdAndCustomerId(Long id, Long customerId);

    Page<Task> findAllByCustomerId(Long id, @Nullable Specification<Task> spec, Pageable pageable);

}
