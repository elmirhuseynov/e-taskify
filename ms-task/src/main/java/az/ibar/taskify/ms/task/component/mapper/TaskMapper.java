package az.ibar.taskify.ms.task.component.mapper;

import az.ibar.taskify.ms.task.model.TaskAssigneeEmail;
import az.ibar.taskify.ms.task.model.dto.request.CreateTaskDto;
import az.ibar.taskify.ms.task.model.dto.response.TaskDto;
import az.ibar.taskify.ms.task.model.entity.Task;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TaskMapper {

    Task dto2Entity(CreateTaskDto taskDto, Long customerId, Long reporterId);

    TaskDto entity2Dto(Task task);

    TaskAssigneeEmail task2AssigneeEmail(Task task);

}
