package az.ibar.taskify.ms.task.configuration;

import az.ibar.taskify.lib.common.component.MessageResolver;
import az.ibar.taskify.lib.common.component.security.AccessDeniedHandlerImpl;
import az.ibar.taskify.lib.common.component.security.AuthenticationEntryPointImpl;
import az.ibar.taskify.lib.common.component.security.JwtConfigurer;
import az.ibar.taskify.lib.common.component.security.TokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.CorsFilter;


@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Import({TokenProvider.class, AccessDeniedHandlerImpl.class, AuthenticationEntryPointImpl.class})
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final String[] SWAGGER_PATHS = new String[]{
            "/v2/api-docs",
            "/v2/swagger.json",
            "/webjars/**",
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/configuration/ui",
            "/configuration/security"
    };

    private final CorsFilter corsFilter;
    private final TokenProvider tokenProvider;
    private final MessageResolver messageResolver;
    private final AccessDeniedHandlerImpl accessDeniedHandler;
    private final AuthenticationEntryPointImpl authenticationEntryPoint;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**")
                .antMatchers(SWAGGER_PATHS)
                .antMatchers("/actuator/health");

    }

    @Override
    @SuppressWarnings("MethodLength")
    protected void configure(HttpSecurity http) throws Exception {
        http
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/auth/logout"))
                .and()
                .csrf()
                .disable()
                .exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler)
                .authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .headers()
                .contentSecurityPolicy("default-src 'self'; frame-src 'self' data:; script-src 'self' " +
                        "'unsafe-inline' 'unsafe-eval' https://storage.googleapis.com; style-src 'self' " +
                        "'unsafe-inline'; img-src 'self' data:; font-src 'self' data:")
                .and()
                .referrerPolicy(ReferrerPolicyHeaderWriter.ReferrerPolicy.STRICT_ORIGIN_WHEN_CROSS_ORIGIN)
                .and()
                .featurePolicy("geolocation 'none'; midi 'none'; sync-xhr 'none'; microphone 'none'; " +
                        "camera 'none'; magnetometer 'none'; gyroscope 'none'; speaker 'none'; " +
                        "fullscreen 'self'; payment 'none'")
                .and()
                .frameOptions()
                .deny()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)
                .apply(jwtConfigurer());
    }

    private JwtConfigurer jwtConfigurer() {
        return new JwtConfigurer(tokenProvider, messageResolver);
    }
}
