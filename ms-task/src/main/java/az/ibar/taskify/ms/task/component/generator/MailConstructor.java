package az.ibar.taskify.ms.task.component.generator;

import az.ibar.taskify.lib.common.model.event.MailEvent;
import az.ibar.taskify.ms.task.model.TaskAssigneeEmail;
import az.ibar.taskify.ms.task.model.dto.response.UserInfoDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MailConstructor {

    private final HtmlGenerator htmlGenerator;

    public MailEvent constructTaskAssigneeEmail(TaskAssigneeEmail email, UserInfoDto assigneeUser) {
        MailEvent mail = new MailEvent();
        mail.setFrom("e.taskify@gmail.com");
        mail.setTo(new String[]{assigneeUser.getEmail()});
        mail.setSubject("Task assignee");
        mail.setText(htmlGenerator.generateTaskAssigneeHtml(email));
        return mail;
    }

}
