package az.ibar.taskify.ms.task.service;

import az.ibar.taskify.lib.common.model.constant.JwtConstants;
import az.ibar.taskify.lib.common.model.dto.GenericSearchDto;
import az.ibar.taskify.lib.common.model.event.MailEvent;
import az.ibar.taskify.lib.common.repository.search.SearchSpecification;
import az.ibar.taskify.lib.common.util.SecurityContextUtils;
import az.ibar.taskify.ms.task.client.IdentityClient;
import az.ibar.taskify.ms.task.component.MessagePublisher;
import az.ibar.taskify.ms.task.component.generator.MailConstructor;
import az.ibar.taskify.ms.task.component.mapper.TaskMapper;
import az.ibar.taskify.ms.task.model.TaskAssigneeEmail;
import az.ibar.taskify.ms.task.model.dto.request.CreateTaskDto;
import az.ibar.taskify.ms.task.model.dto.response.TaskDto;
import az.ibar.taskify.ms.task.model.dto.response.UserInfoDto;
import az.ibar.taskify.ms.task.model.entity.Task;
import az.ibar.taskify.ms.task.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

import static az.ibar.taskify.lib.common.model.constant.MessagingQueueConstants.EXCHANGE_MAIL;
import static az.ibar.taskify.lib.common.model.constant.MessagingQueueConstants.ROUTING_KEY_MAIL;
import static az.ibar.taskify.lib.common.util.RequestUtils.getLanguage;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskMapper taskMapper;
    private final MailConstructor mailConstructor;
    private final IdentityClient identityClient;
    private final TaskRepository taskRepository;
    private final MessagePublisher messagePublisher;
    private final HttpServletRequest servletRequest;

    public void createTask(CreateTaskDto taskDto) {
        Long customerId = SecurityContextUtils.getCustomerId();
        Task task = taskMapper.dto2Entity(taskDto, customerId, SecurityContextUtils.getUserId());
        taskRepository.save(task);
    }

    public void assignTask(Long taskId, Long assigneeId) {
        Long customerId = SecurityContextUtils.getCustomerId();

        Task task = taskRepository.findByIdAndCustomerId(taskId, customerId)
                .orElseThrow(); //TODO add locale exp
        task.setAssigneeId(assigneeId);

        UserInfoDto assigneeUser = identityClient.getUserInfo(getAuthorizationHeader(),
                getLanguage(),
                assigneeId
        );
        task = taskRepository.save(task);

        TaskAssigneeEmail taskAssigneeEmail = taskMapper.task2AssigneeEmail(task);
        MailEvent mail = mailConstructor.constructTaskAssigneeEmail(taskAssigneeEmail, assigneeUser);
        messagePublisher.publish(EXCHANGE_MAIL, ROUTING_KEY_MAIL, mail);
    }

    public Page<TaskDto> searchAllTasks(GenericSearchDto filter, Pageable pageable) {
        Long customerId = SecurityContextUtils.getCustomerId();
        return taskRepository.findAllByCustomerId(
                customerId,
                new SearchSpecification<>(filter.getCriteria()),
                pageable
        ).map(taskMapper::entity2Dto);
    }

    private String getAuthorizationHeader() {
        return servletRequest.getHeader(JwtConstants.AUTHORIZATION_HEADER);
    }

}
