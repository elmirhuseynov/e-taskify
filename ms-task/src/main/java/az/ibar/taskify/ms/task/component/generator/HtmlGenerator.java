package az.ibar.taskify.ms.task.component.generator;

import az.ibar.taskify.ms.task.model.TaskAssigneeEmail;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.util.Map;

@Component
public class HtmlGenerator {

    private final SpringTemplateEngine templateEngine;

    public HtmlGenerator(SpringTemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public String generateHtml(Map<String, Object> variables, String templateName) {
        Context context = new Context();
        context.setVariables(variables);

        return templateEngine.process(templateName, context);
    }

    public String generateTaskAssigneeHtml(TaskAssigneeEmail email) {
        Context context = new Context();
        context.setVariable("email", email);

        return templateEngine.process("assignee-task-mail", context);
    }
}
