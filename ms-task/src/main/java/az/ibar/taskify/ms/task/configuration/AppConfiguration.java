package az.ibar.taskify.ms.task.configuration;

import az.ibar.taskify.lib.common.component.MessageResolver;
import az.ibar.taskify.lib.common.configuration.CommonConfiguration;
import az.ibar.taskify.lib.common.configuration.LocaleConfiguration;
import az.ibar.taskify.lib.common.configuration.WebConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({CommonConfiguration.class, WebConfiguration.class, LocaleConfiguration.class, MessageResolver.class})
public class AppConfiguration {
}
