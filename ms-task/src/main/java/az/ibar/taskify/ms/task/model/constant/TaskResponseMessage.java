package az.ibar.taskify.ms.task.model.constant;

import az.ibar.taskify.lib.common.model.constant.ResponseMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TaskResponseMessage implements ResponseMessage {

    TASK_STATUS_CHANGING_NOT_ALLOWED("error.taskStatusChangingNotAllowed"),
    TASK_NOT_FOUND("error.taskNotFound");

    private final String messageKey;

}
