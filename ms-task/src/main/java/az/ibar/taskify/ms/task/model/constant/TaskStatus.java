package az.ibar.taskify.ms.task.model.constant;

import az.ibar.taskify.ms.task.component.transition.DoneTransition;
import az.ibar.taskify.ms.task.component.transition.InProgressTransition;
import az.ibar.taskify.ms.task.component.transition.TestingTransition;
import az.ibar.taskify.ms.task.component.transition.TodoTransition;

import java.util.Arrays;
import java.util.List;

public enum TaskStatus {

    TODO(InProgressTransition.NAME),
    IN_PROGRESS(TestingTransition.NAME, TodoTransition.NAME),
    TESTING(DoneTransition.NAME, InProgressTransition.NAME, TodoTransition.NAME),
    DONE();

    private final List<String> transitions;

    TaskStatus(String... transitions) {
        this.transitions = Arrays.asList(transitions);
    }

    public List<String> getTransitions() {
        return transitions;
    }

    /*public static Optional<TaskStatus> checkAllowedTransition(TaskTransition taskTransition, //TESTING
                                                              TaskStatus taskStatus) {//TODO
        return Stream.of(values())
                .filter(s -> s.getTransitions().contains(taskTransition.getStatus()))
                .filter(s -> s.getTransitions().contains(taskStatus))
                .findFirst();
    }*/

}
