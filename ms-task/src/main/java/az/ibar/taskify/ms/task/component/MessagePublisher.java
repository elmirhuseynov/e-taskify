package az.ibar.taskify.ms.task.component;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
@RequiredArgsConstructor
public class MessagePublisher {

    private final RabbitTemplate rabbitTemplate;

    public <T extends Serializable> void publish(String exchange, String routingKey, T event) {
        rabbitTemplate.convertAndSend(exchange, routingKey, event);
    }

}
