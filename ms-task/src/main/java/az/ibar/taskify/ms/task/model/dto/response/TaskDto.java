package az.ibar.taskify.ms.task.model.dto.response;

import az.ibar.taskify.ms.task.model.constant.TaskStatus;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TaskDto {

    private Long id;

    private Long reporterId;

    private Long assigneeId;

    private String title;

    private String description;

    private LocalDateTime deadline;

    private TaskStatus taskStatus;

}
