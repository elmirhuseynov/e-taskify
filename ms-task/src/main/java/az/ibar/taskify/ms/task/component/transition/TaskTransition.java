package az.ibar.taskify.ms.task.component.transition;

import az.ibar.taskify.ms.task.model.constant.TaskStatus;
import az.ibar.taskify.ms.task.model.dto.response.TaskDto;

public interface TaskTransition {

    String getName();

    TaskStatus getStatus();

    void applyProcessing(TaskDto taskDto);

}
