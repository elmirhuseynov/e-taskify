package az.ibar.taskify.ms.task.exception;

import az.ibar.taskify.lib.common.exception.BadRequestException;
import az.ibar.taskify.ms.task.model.constant.TaskResponseMessage;

public class TaskStatusChangingNotAllowedException extends BadRequestException {

    private static final long serialVersionUID = 1L;

    public TaskStatusChangingNotAllowedException(Object[] objects) {
        super(TaskResponseMessage.TASK_STATUS_CHANGING_NOT_ALLOWED, objects);
    }
}
