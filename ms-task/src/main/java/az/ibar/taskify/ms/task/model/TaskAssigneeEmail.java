package az.ibar.taskify.ms.task.model;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class TaskAssigneeEmail {

    private String createdBy;
    private String assignee;
    private String title;
    private String description;
    private LocalDateTime createdDate;
    private LocalDate deadline;

}
