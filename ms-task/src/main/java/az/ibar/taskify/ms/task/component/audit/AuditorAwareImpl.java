package az.ibar.taskify.ms.task.component.audit;

import az.ibar.taskify.lib.common.util.SecurityContextUtils;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of(SecurityContextUtils.getOptionalFullName().orElse("Anonymous"));
    }
}
