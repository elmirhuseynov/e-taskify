package az.ibar.taskify.ms.task.model.dto.request;

import az.ibar.taskify.ms.task.model.constant.TaskStatus;
import lombok.Data;

@Data
public class ChangeTaskStatusDto {

    private Long taskId;
    private TaskStatus taskStatus;

}
