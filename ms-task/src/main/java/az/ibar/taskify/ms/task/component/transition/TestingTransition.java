package az.ibar.taskify.ms.task.component.transition;

import az.ibar.taskify.ms.task.model.constant.TaskStatus;
import az.ibar.taskify.ms.task.model.dto.response.TaskDto;
import org.springframework.stereotype.Component;

@Component
public class TestingTransition implements TaskTransition {

    public static final String NAME = "TESTING";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public TaskStatus getStatus() {
        return TaskStatus.TESTING;
    }

    @Override
    public void applyProcessing(TaskDto taskDto) {
        //Apply processing
    }
}
