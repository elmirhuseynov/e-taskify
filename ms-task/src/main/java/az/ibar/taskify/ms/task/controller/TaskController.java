package az.ibar.taskify.ms.task.controller;

import az.ibar.taskify.lib.common.model.dto.GenericSearchDto;
import az.ibar.taskify.ms.task.model.dto.request.ChangeTaskStatusDto;
import az.ibar.taskify.ms.task.model.dto.request.CreateTaskDto;
import az.ibar.taskify.ms.task.model.dto.response.TaskDto;
import az.ibar.taskify.ms.task.service.TaskService;
import az.ibar.taskify.ms.task.service.TaskTransitionService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;
    private final TaskTransitionService taskTransitionService;

    @PostMapping
    public void createTask(@Valid @RequestBody CreateTaskDto taskDto) {
        taskService.createTask(taskDto);
    }

    @PutMapping("/assignee")
    public void assignTask(@NotNull @RequestParam Long taskId,
                           @NotNull @RequestParam Long assigneeId) {

        taskService.assignTask(taskId, assigneeId);
    }

    @PostMapping("/search")
    public Page<TaskDto> searchAllTasks(@RequestBody GenericSearchDto filter, Pageable pageable) {
        return taskService.searchAllTasks(filter, pageable);
    }

    @PostMapping("/statusChanging")
    public void changeTaskStatus(@Valid @RequestBody ChangeTaskStatusDto changeTaskStatusDto) {
        taskTransitionService.changeTaskStatus(changeTaskStatusDto);
    }

}
