package az.ibar.taskify.ms.task.component.transition;

import az.ibar.taskify.ms.task.model.constant.TaskStatus;
import az.ibar.taskify.ms.task.model.dto.response.TaskDto;
import org.springframework.stereotype.Component;

@Component
public class InProgressTransition implements TaskTransition {

    public static final String NAME = "IN_PROGRESS";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public TaskStatus getStatus() {
        return TaskStatus.IN_PROGRESS;
    }

    @Override
    public void applyProcessing(TaskDto taskDto) {
        //Apply processing
    }
}
