package az.ibar.taskify.ms.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("az.ibar.taskify.ms.task.client")
public class MsTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsTaskApplication.class, args);
    }

}
