package az.ibar.taskify.ms.task.service;

import az.ibar.taskify.lib.common.exception.NotFoundException;
import az.ibar.taskify.ms.task.model.constant.TaskResponseMessage;

public class TaskNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 47623542L;

    public TaskNotFoundException() {
        super(TaskResponseMessage.TASK_NOT_FOUND);
    }
}
