package az.ibar.taskify.ms.task.model.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AssignTaskDto {

    @NotNull
    private Long assigneeId;

}
