package az.ibar.taskify.ms.task.client;

import az.ibar.taskify.ms.task.model.dto.response.UserInfoDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "ms-identity", url = "${ms.identity.url}")
public interface IdentityClient {

    @GetMapping("/user/info")
    UserInfoDto getUserInfo(@RequestHeader("Authorization") String authorization,
                            @RequestHeader("Accept-Language") String lang,
                            @RequestParam Long userId);
}
